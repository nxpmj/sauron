export interface DomainDataAttributes {
	name: string;
	bidCount: string;
	estimatedValue: string;
	latestPriceText: string;
	timeLeft: string;
	bidUrl: string;
	da: string;
	pa: string;
	ss: string;
	age: string;
	tf: string;
	cf: string;
	alexaRank: string;
	organicKeyword: string;
	organicTraffic: string;
	searchCount: string;
}
