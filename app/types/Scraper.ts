export interface GoDaddyDomainData {
	bidCount: string;
	estimatedValue: string;
	latestPriceText: string;
	timeLeft: string;
	bidUrl: string;
}

export interface SpamZillaDomainFilteredData {
	da: string;
	pa: string;
	ss: string;
	age: string;
	tf: string;
	cf: string;
	alexaRank: string;
	timeLeft: string;
	timeEndDate:Date;
	latestPriceText:string;
	vendorName:string;
}

export interface SpamZillaDomainData {
	da: string;
	pa: string;
	ss: string;
	age: string;
	tf: string;
	cf: string;
	alexaRank: string;
}

export interface AhrefsDomainData {
	organicKeyword: string;
	organicTraffic: string;
	dataAhref: any;
}

export interface GoogleSerpDomainData {
	searchCount: string;
}
