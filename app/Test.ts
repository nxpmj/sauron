import * as envData from '../config/Env.json';
import { ApiVersion, Config, Env, ErrorHandler, SetEnv } from './core';
SetEnv(envData);
import { useExpressServer } from '@wavecore/routing-controllers';
import * as express from 'express';
import * as iconv from 'iconv-lite';
import * as Job from './jobs/Job';
import WaBotService from './services/WaBotService';
import ScraperService from './services/scraper/ScraperService';
import * as moment from 'moment';
import { DomainData } from './models/DomainData';
import { Op } from 'sequelize';
import BookingService from './services/BookingService';
import DomainDataService from './services/DomainDataService';
import AhrefsService from './services/scraper/AhrefsService';
import { FilterService } from './services/FilterService';
import { RankService } from './services/RankService';
import { DataAhrefs } from './models/DataAhrefs';
import { AgeService } from './services/scraper/AgeService';
import EmailService from './services/notification/EmailService';
import NotificationService from './services/notification/NotificationService';
iconv.encodingExists('foo');

export async function  test() {
	SetEnv(envData);
	Config.init();


	ScraperService.CheckStatsTask('zoomtell.com ')
}
test();
