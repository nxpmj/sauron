import { Env } from '../core';
import * as schedule from 'node-schedule';
import ScraperService from '../services/scraper/ScraperService';
import BookingService from '../services/BookingService';

export class UpdateDeadline {
	static async init() {
		schedule.scheduleJob('30 */3 * * *', () => {
			BookingService.updateDeadline(9);
		});
		schedule.scheduleJob('0 10,22 * * *', () => {
			BookingService.incomingDeadline(null, 'INCOMING', 18);
			BookingService.updateDeadline(24);
		});
		schedule.scheduleJob('0 * * * *', () => {
			BookingService.incomingDeadline(null, 'INCOMING', 1);
			BookingService.updateDeadline(1);
		});

		if (Env().environment === 'production') {
			BookingService.updateDeadline(24);
		}
	}
}
