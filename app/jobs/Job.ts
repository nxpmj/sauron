import { ScrapDomains } from './ScrapDomains';
import { TrackPrice } from './TrackPrice';
import { UpdateDeadline } from './UpdateDeadline';

export function init() {
	ScrapDomains.init();
	UpdateDeadline.init();
	TrackPrice.init();
}
