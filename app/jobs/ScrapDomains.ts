import { Env } from '../core';
import * as schedule from 'node-schedule';
import ScraperService from '../services/scraper/ScraperService';

export class ScrapDomains {
	static async init() {
		schedule.scheduleJob('0 */6 * * *', () => {
			this.execute();
		});

		if (Env().environment === 'production') {
			//this.execute();
		}
	}

	static async execute() {
		await ScraperService.ScrapDomainDataTask();
	}
}
