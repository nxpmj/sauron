import { Env } from '../core';
import * as schedule from 'node-schedule';
import ScraperService from '../services/scraper/ScraperService';
import BookingService from '../services/BookingService';

export class TrackPrice {
	static async init() {
		schedule.scheduleJob('0 * * * *', () => {
			BookingService.trackPriceAll();
		});

		if (Env().environment === 'production') {
			BookingService.trackPriceAll();
		}

	}
}
