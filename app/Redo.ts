import * as envData from '../config/Env.json';
import { ApiVersion, Config, Env, ErrorHandler, SetEnv } from './core';
SetEnv(envData);
import { useExpressServer } from '@wavecore/routing-controllers';
import * as express from 'express';
import * as iconv from 'iconv-lite';
import * as moment from 'moment';
import { Op } from 'sequelize';
import * as Job from './jobs/Job';
import { DataAhrefs } from './models/DataAhrefs';
import { DomainData } from './models/DomainData';
import BookingService from './services/BookingService';
import DomainDataService from './services/DomainDataService';
import { FilterService } from './services/FilterService';
import EmailService from './services/notification/EmailService';
import NotificationService from './services/notification/NotificationService';
import { RankService } from './services/RankService';
import { AgeService } from './services/scraper/AgeService';
import AhrefsService from './services/scraper/AhrefsService';
import ScraperService from './services/scraper/ScraperService';
import { WebSeoCheckerService } from './services/scraper/WebSeoCheckerService';
import WaBotService from './services/WaBotService';
import GoDaddyService from './services/scraper/GoDaddyService';
import FixerService from './services/FixerService';
iconv.encodingExists('foo');

export async function  test() {
	SetEnv(envData);
	Config.init();


	FixerService.detailsFixerById([136206	,
		121188	,
		136602	,
		119410	,
		137447	,
		136983	,
		137083	,
		136039	,
		137786	,
		135571	,
		136100	,
		136237	,
		135479	,
		136948	,
		136977	,
		135477	,
		136808	,
		137375	,
		137335	,
		137634	,
		137251	,
		137317	,
		137434	,
		127349	,
		136766	,
		136639	,
		137166	,
		136824	,
		136094	,
		136062	,
		136208	,
		136236	,
		135540	,
		136647	,
		136188	,
		136063	,
		127387	,
		136972	,
		136651	,
		137459	,
		137418	,
		136592	,
		136578	,
		136910	,
		136818	,
		137195	,
		137720	,
		136990	,
		137420	,
		136196	,
		137243	,
		136466	,
		137953	,
		136954	,
		136912	,
		137564	,
		136794	,
		137956	,
		134835	,
		136189	,
		137563	,
		137732	,
		136796	,
		136080	,
		135863	,
		136773	,
		135870	,
		136815	,
		136659	,
		136545	,
		136793	,
		138029	,
		135888	,
		136909	,
		136192	,
		135884	,
		135876	,
		136547	,
		135758	,
		136816	,
		136819	,
		135885	,
		137734	,
		137617	,
		134988	,
		135759	,
		135893	,
		137574	,
		136191	,
		136795	,
		136822	,
		136460	,
	]);
	// const queryDuplicateDomain = `SELECT * from DomainData where name IN (SELECT name FROM DomainData GROUP BY NAME, goDaddyId HAVING COUNT(*)>1) ORDER BY NAME ASC, id desc`;
	/*domainDatas.forEach( domainData => {
		domainDataList[domainData.name] = domainData;
	})
	 await ScraperService.CheckStats(domainDataList, true);*/

	console.log();
	// ScraperService.ScrapDomainDataTask();
}
test();
