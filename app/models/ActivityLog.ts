import { BaseSequelizeModel } from '../core';
import { Column, Table } from 'sequelize-typescript';

@Table
export class ActivityLog extends BaseSequelizeModel {
	@Column
	description: string;
}
