import { BaseSequelizeModel } from '../core';
import { Column, Table } from 'sequelize-typescript';

@Table
export class Contact extends BaseSequelizeModel {
	@Column
	name: string;

	@Column
	phone: string;

	@Column
	group: string;

}
