import { BaseSequelizeModel } from '../core';
import { BelongsTo, Column, ForeignKey, HasMany, HasOne, Table } from 'sequelize-typescript';
import { Contact } from './Contact';
import { DomainData } from './DomainData';
import { DataWebSeoChecker } from './DataWebSeoChecker';
import { DataAhrefsTraffic } from './DataAhrefsTraffic';

@Table
export class DataAhrefs extends BaseSequelizeModel {
	@Column ahrefsRank: number;
	@Column ur: number;
	@Column dr: number;
	@Column backlinks: number;
	@Column referringDomains: number;
	@Column organicKeywords: number;
	@Column organicTraffic: number;
	@Column trafficValue: number;
	@Column totalTraffic: number;
	@Column isExGambling: boolean;
	@Column exGamblingKeywords: string;
	@Column isDownward: boolean;
	@Column trafficSummary: string;
	@Column trafficValueSummary: string;
	@Column keywordSummary: string;

	@ForeignKey(() => DomainData)
	@Column
	domainDataId: number;
	@BelongsTo(() => DomainData)
	domainData: DomainData;

	@HasMany(() => DataAhrefsTraffic)
	dataAhrefsTraffics: DataAhrefsTraffic[];

}
