import { BelongsTo, Column, ForeignKey, HasOne, Table } from 'sequelize-typescript';
import { BaseSequelizeModel } from '../core';
import { Contact } from './Contact';
import { DataWebSeoChecker } from './DataWebSeoChecker';
import { DataAhrefs } from './DataAhrefs';

@Table
export class DomainData extends BaseSequelizeModel {
	@Column name: string;
	@Column bidCount: string;
	@Column estimatedValue: string;
	@Column latestPrice: number;
	@Column latestPriceText: string;
	@Column deliveryDays: number;
	@Column buyNowPrice: string;
	@Column timeLeft: string;
	@Column timeEndDate: Date;
	@Column bidUrl: string;
	@Column da: string;
	@Column pa: string;
	@Column ss: string;
	@Column age: string;
	@Column tf: string;
	@Column cf: string;
	@Column alexaRank: string;
	@Column organicKeyword: string;
	@Column organicTraffic: string;
	@Column searchCount: string;
	@Column rankLop: string;
	@Column trackPrice: boolean;
	@Column forceCheck: boolean;
	@Column vendorId: string;
	@Column vendorName: string;
	@Column aggregator: string;

	@ForeignKey(() => Contact)
	@Column
	bookContactId: number;

	@BelongsTo(() => Contact)
	bookContact: Contact;

	@HasOne(() => DataWebSeoChecker)
	dataWebSeoChecker: DataWebSeoChecker;
	@HasOne(() => DataAhrefs)
	dataAhrefs: DataAhrefs;
}
