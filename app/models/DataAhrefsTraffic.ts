import { BaseSequelizeModel } from '../core';
import { BelongsTo, Column, ForeignKey, HasMany, Table } from 'sequelize-typescript';
import { Contact } from './Contact';
import { DomainData } from './DomainData';
import { DataAhrefs } from './DataAhrefs';
import { DataAhrefsTrafficDetails } from './DataAhrefsTrafficDetails';

@Table
export class DataAhrefsTraffic extends BaseSequelizeModel {
	@Column traffic: number;
	@Column value: number;
	@Column keywords: number;
	@Column rd: number;
	@Column url: string;
	@Column topKeywords: string;
	@Column topKeywordsVolume: number;
	@Column topKeywordsPosition: number;

	@ForeignKey(() => DomainData)
	@Column
	domainDataId: number;
	@BelongsTo(() => DomainData)
	domainData: DomainData;

	@ForeignKey(() => DataAhrefs)
	@Column
	dataAhrefsId: number;
	@BelongsTo(() => DataAhrefs)
	dataAhrefs: DataAhrefs;

	@HasMany(() => DataAhrefsTrafficDetails)
	dataAhrefsTrafficDetails: DataAhrefsTrafficDetails[];

}
