import { BaseSequelizeModel } from '../core';
import { BelongsTo, Column, ForeignKey, Table } from 'sequelize-typescript';
import { Contact } from './Contact';
import { DomainData } from './DomainData';
import { DataAhrefs } from './DataAhrefs';
import { DataAhrefsTraffic } from './DataAhrefsTraffic';

@Table
export class DataAhrefsTrafficDetails extends BaseSequelizeModel {
	@Column keyword: string;
	@Column position: number;
	@Column volume: number;
	@Column kd: number;
	@Column cpc: number;
	@Column traffic: number;
	@Column results: number;
	@Column updateDate: string;
	@Column country: string;

	@ForeignKey(() => DomainData)
	@Column
	domainDataId: number;

	@BelongsTo(() => DomainData)
	domainData: DomainData;

	@ForeignKey(() => DataAhrefsTraffic)
	@Column
	dataAhrefsTrafficId: number;

	@BelongsTo(() => DataAhrefsTraffic)
	dataAhrefsTraffic: DataAhrefsTraffic;

}
