import { BaseSequelizeModel } from '../core';
import { BelongsTo, Column, ForeignKey, Table } from 'sequelize-typescript';
import { Contact } from './Contact';
import { DomainData } from './DomainData';

@Table
export class DataWebSeoChecker extends BaseSequelizeModel {
	@Column categories: string;
	@Column da: number;
	@Column pa: number;
	@Column totalBacklinks: number;
	@Column qualityBacklinks: number;
	@Column qualityBacklinksPercentage: number;
	@Column mozTrust: number;
	@Column spamScore: number;
	@Column trustFlow: number;
	@Column citationFlow: number;
	@Column indexedUrls: number;
	@Column doFollowLinksPercentage: number;
	@Column noFollowLinksPercentage: number;

	@ForeignKey(() => DomainData)
	@Column
	domainDataId: number;

	@BelongsTo(() => DomainData)
	domainData: DomainData;

}
