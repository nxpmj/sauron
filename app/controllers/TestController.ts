import { Body, Get, JsonController, Req } from '@wavecore/routing-controllers';
import SpamZillaService from '../services/scraper/SpamZillaService';
import { success } from '../core';

@JsonController('/test')
export class TestController {
	@Get('/test-spamzilla')
	async test(@Body() input: any, @Req() req: any) {
		const result = []

		return success({ result });
	}
}
