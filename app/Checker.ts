import * as envData from '../config/Env.json';
import { ApiVersion, Config, Env, ErrorHandler, SetEnv } from './core';
SetEnv(envData);
import { useExpressServer } from '@wavecore/routing-controllers';
import * as express from 'express';
import * as iconv from 'iconv-lite';
import * as Job from './jobs/Job';
import WaBotService from './services/WaBotService';
import ScraperService from './services/scraper/ScraperService';
import * as moment from 'moment';
import { DomainData } from './models/DomainData';
import { Op } from 'sequelize';
import BookingService from './services/BookingService';
import DomainDataService from './services/DomainDataService';
import AhrefsService from './services/scraper/AhrefsService';
import { FilterService } from './services/FilterService';
import { RankService } from './services/RankService';
import { DataAhrefs } from './models/DataAhrefs';
import { AgeService } from './services/scraper/AgeService';
import EmailService from './services/notification/EmailService';
import NotificationService from './services/notification/NotificationService';
import { WebSeoCheckerService } from './services/scraper/WebSeoCheckerService';
iconv.encodingExists('foo');

export async function  test() {
	SetEnv(envData);
	Config.init();


	let domainList = [];
	const fs = require('fs')

	try {
		const data = fs.readFileSync('checkerList.txt', 'utf8');
		domainList = [...data.split('\r\n')]
		console.log(data)
	} catch (err) {
		console.error(err)
	}

	let domainDataList: Record<string, DomainData> = {};
	for(const domainName of domainList){
		if (!domainName) continue;
		const domainData = await DomainData.findOrCreate({
			where: {
				name: domainName
			}
		});
		domainDataList[domainData[0].name] = domainData[0];
	}


	domainDataList = await ScraperService.CheckStats(domainDataList, true);
	// await WebSeoCheckerService.updateMozData(domainDataList);

	console.log();
}
test();
