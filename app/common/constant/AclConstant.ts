import { aclCrudGenerator, aclGenerator } from '../../core/util/CommonUtil';

export const AclConstant = {
	super: {
		structured: aclGenerator(
			{
				DASHBOARD: ['DASHBOARD_SNAPSHOT_POST'],
				REPORT: ['REPORT_PROFIT_POST'],
				INVOICE: [
					'INVOICE_FILE_UPLOAD_PATCH',
					'INVOICE_APPROVE_PUT',
					'INVOICE_CHECK_PAYMENT_POST',
					'INVOICE_NOTIFY_MERCHANT_POST',
				],
				MERCHANT: ['MERCHANT_OPTION_LIST_GET'],
				MERCHANT_SITE: ['MERCHANT_SITE_OPTION_LIST_GET'],
				CONFIG: ['CONFIG_PRODUCT_GET', 'CONFIG_PRODUCT_POST'],
			},
			{ ...aclCrudGenerator('ADMIN', ['LIST', 'DETAILS', 'CREATE', 'UPDATE']) },
			{ ...aclCrudGenerator('INVOICE', ['LIST', 'DETAILS', 'UPDATE']) },
			{ ...aclCrudGenerator('MERCHANT', ['LIST']) },
			{ ...aclCrudGenerator('MERCHANT_SITE', ['LIST', 'DETAILS', 'CREATE', 'UPDATE', 'DELETE']) }
		),
	},
	user: {
		structured: aclGenerator(
			{
				REPORT: ['REPORT_PROFIT_POST'],
				INVOICE: [
					'INVOICE_FILE_UPLOAD_PATCH',
					'INVOICE_APPROVE_PUT',
					'INVOICE_CHECK_PAYMENT_POST',
					'INVOICE_NOTIFY_MERCHANT_POST',
				],
				MERCHANT: ['MERCHANT_OPTION_LIST_GET'],
				MERCHANT_SITE: ['MERCHANT_SITE_OPTION_LIST_GET'],
			},
			{ ...aclCrudGenerator('INVOICE', ['LIST', 'DETAILS', 'UPDATE']) }
		),
	},
	support: {
		structured: aclGenerator(
			{
				DASHBOARD: ['DASHBOARD_SNAPSHOT_POST'],
				MERCHANT: ['MERCHANT_OPTION_LIST_GET'],
				MERCHANT_SITE: ['MERCHANT_SITE_OPTION_LIST_GET'],
			},
			{ ...aclCrudGenerator('INVOICE', ['LIST', 'DETAILS']) }
		),
	},
	admin: {
		structured: aclGenerator(
			{
				DASHBOARD: ['DASHBOARD_SNAPSHOT_POST'],
				UPLOAD: ['UPLOAD_FILE_PUT'],
				MERCHANT: ['MERCHANT_OPTION_LIST_GET'],
				MERCHANT_SITE: ['MERCHANT_SITE_OPTION_LIST_GET'],
			},
			aclCrudGenerator('ADMIN', ['LIST', 'CREATE', 'UPDATE', 'DETAILS', 'DELETE', 'RESETPASSWORD'])
		),
		transaction: aclGenerator({
			TRANSACTION_SEPULSA: [
				'TRANSACTION_SEPULSA_LIST_POST',
				'TRANSACTION_SEPULSA_CREATE_PUT',
				'TRANSACTION_SEPULSA_DETAIL_PATCH',
			],
			TRANSACTION_MERCHANT: [
				'TRANSACTION_MERCHANT_LIST_POST',
				'TRANSACTION_MERCHANT_CREATE_PUT',
				'TRANSACTION_MERCHANT_DETAIL_PATCH',
			],
		}),
		admin: aclGenerator(aclCrudGenerator('ADMIN', ['LIST', 'CREATE', 'UPDATE', 'DETAILS', 'DELETE', 'RESETPASSWORD'])),
	},
	default: ['TEST_SEND_EMAIL_GET', 'PROFILE_PASSWORD_RESET_POST', 'PROFILE_DETAILS_POST', 'PROFILE_UPDATE_PATCH'],
};
