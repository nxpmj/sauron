import { BaseSequelizeModel } from '../../core';
import { Column, Table } from 'sequelize-typescript';

@Table
export class Admin extends BaseSequelizeModel {
	@Column
	email: string;

	@Column
	password: string;

	@Column
	name: string;

	@Column
	role: string;

	@Column
	lastLogin: string;

	@Column
	merchantSites: string;

	@Column
	status: string;

	@Column
	createdBy: number;

	@Column
	updatedBy: number;

	static findByEmail(email) {
		return this.findOne({
			where: {
				email,
			},
		});
	}
}
