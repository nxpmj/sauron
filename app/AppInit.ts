import * as envData from '../config/Env.json';
import { ApiVersion, Config, ErrorHandler, SetEnv } from './core';
SetEnv(envData);
import { useExpressServer } from '@wavecore/routing-controllers';
import * as express from 'express';
import * as iconv from 'iconv-lite';
import * as Job from './jobs/Job';
import WaBotService from './services/WaBotService';
iconv.encodingExists('foo');

export function createServer() {
	SetEnv(envData);
	Config.init();

	const app = express();
	app.set('x-powered-by', false);
	app.set('etag', false);
	const routingControllersOption = {
		cors: true,
		defaultErrorHandler: false,
		middlewares: [ErrorHandler, ApiVersion],
		controllers: [__dirname + '/controllers/*.js'],
	};

	const bodyParser = require('body-parser');
	app.use(bodyParser.json({ limit: '50mb' }));
	app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));

	useExpressServer(app, routingControllersOption);
	Job.init();
	WaBotService.initListener();

	return app;
}
