import * as moment from 'moment';
import ActivityLogService from './ActivityLogService';

class LoggerService {
	static async LogActivity(activity: string, error?: any) {
		console.log(`[${moment().format('DD-MM-YYYY hh:mm:ss')}]`, activity);
		if (error) {
			console.error(error);
		}
		await ActivityLogService.Save(activity);
	}
}

export default LoggerService;
