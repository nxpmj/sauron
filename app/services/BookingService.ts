import { Env, System } from '../core';
import { AxiosService } from '../core/service/AxiosService';
import { Contact } from '../models/Contact';
import WaService from './notification/WaService';
import { DomainData } from '../models/DomainData';
import { Op } from 'sequelize';
import * as moment from 'moment';
import { RankService } from './RankService';
import GoDaddyService from './scraper/GoDaddyService';

export default class BookingService {
	static defaultGroupName = 'Perguruan SEO';
	static runningTrackPrice = [];

	/*public static async verifyBooking(messageObj){
		this.Send('Confirm Booking');
	}*/
	public static async confirmBooking(messageObj, domain: string){
		const domainData = await DomainData.findOne({
			where: {
				name: domain
			},
			include: [Contact],
			order: [ ['id', 'DESC'] ]
		});
		if (!domainData){
			WaService.Send(`${domain} is not in the list`, messageObj);
		} else {
			const phone = messageObj.simplify.fromId.split('@')[0].split(':')[0];
			const contact = await Contact.findOrCreate({
				where: {
					phone
				}
			});
			domainData.bookContactId = contact[0].id;
			await domainData.save();

			const message = domainData.bookContact ? `Successfully steal from @${domainData.bookContact.phone} !` :`Successfully booked!`;

			WaService.Send(message, messageObj);
		}
	}
	/*public static async verifyCancelBooking(messageObj){
		this.Send(``);
	}*/
	public static async confirmCancelBooking(messageObj, domain: string){
		const phone = messageObj.simplify.fromId.split('@')[0].split(':')[0];
		const contact = await Contact.findOrCreate({
			where: {
				phone
			}
		})
		const domainData = await DomainData.findOne({
			where: {
				name: domain,
				bookContactId: {[Op.ne]: null}
			},
			include: [Contact],
			order: [ ['id', 'DESC'] ]
		});
		if (!domainData){
			WaService.Send(`${domain} is not in the list`, messageObj);
		} else if (!domainData.bookContact){
			WaService.Send(`${domain} is not booked!`, messageObj);
		/*} else if (domainData.bookContactId != contact[0].id){
			WaService.Send(`${domain} is not booked by you!`, messageObj);*/
		} else {
			domainData.bookContactId = null;
			domainData.save();
			WaService.Send(`Booking cancelled!`, messageObj);
		}
	}
	public static async incomingDeadline(messageObj, type: string = 'INCOMING', withinHourEnd?: number){
		let endDate = moment().endOf('day').add(type == 'ALL' ? 9999 : 18, 'hours');
		if (withinHourEnd){
			endDate = moment().add(withinHourEnd, 'hours');
		}
		let message = '';

		if (type == 'INCOMING' || type == 'ALL'){
			const domainDatas = await this.getIncomingDomainData(endDate);
			domainDatas.forEach( domainData => {

				message += `${moment(domainData.timeEndDate).format('DD MMM HH:mm')} ${domainData.name} - ${domainData.rankLop ?? '?'} - $${domainData.latestPrice ?? '???'}\n`
				// message += ` @${domainData.bookContact.phone} \n\n`
			});
		}

		if (type != 'INCOMING'){
			const domainDataBuyNows = await DomainData.findAll({
				where:{
					timeEndDate: {[Op.eq]: null},
					bookContactId: {[Op.ne]: null}
				},
				order: [
					['estimatedValue', 'ASC'],
					['id', 'ASC'],
				],
				include: [Contact]
			});
			domainDataBuyNows.forEach( domainData => {
				const price = domainData.estimatedValue ? parseInt(domainData.estimatedValue.replace(',','').replace('$','')) : 0;
				if (type == 'BUYNOW5K' && price > 5000) return;

				let priceInK: any = (parseInt( (price/100).toString() )).toString();
				if (priceInK.length == 1) {
					priceInK = `0.${priceInK}`
				}
				else {
					if(priceInK.slice(-1) == '0') priceInK = priceInK.slice(0, -1);
					else priceInK = priceInK.slice(0, -1) + '.' + priceInK.slice(-1);
				}
				message += `$${priceInK}K ${domainData.name} - ${domainData.rankLop ?? '?'}\n`
			});

			if(domainDataBuyNows.length == 0) message = message.slice(0, -2);
			else message = message.slice(0, -1);
		} else {
			message = message.slice(0, -2);
		}

		WaService.Send(message, messageObj);
	}
	public static async getIncomingDomainData(endDate){
		return await DomainData.findAll({
			where:{
				timeEndDate: {
					[Op.between]: [moment().toDate(), endDate.toDate()]
				},
				bookContactId: {[Op.ne]: null}
			},
			order: [
				['timeEndDate', 'ASC'],
			],
			include: [Contact]
		});
	}

	public static async infoUrl(messageObj, domain: string){
		const domainData = await DomainData.findOne({
			where: {
				name: domain
			},
			order: [ ['id', 'DESC'] ]
		});
		if (domainData){
			if (!domainData.bidUrl || domainData.bidUrl == '-'){
				await GoDaddyService.updateBidUrl(domainData);
			}
			if (domainData.bidUrl && domainData.bidUrl != '-'){
				WaService.Send(domainData.bidUrl, messageObj);
			}
		}
	}

	public static async updateDeadline(nextXHours: number){
		const endDate = moment().endOf('day').add(999, 'day').add(nextXHours, 'hours');
		const domainDatas = await this.getIncomingDomainData(endDate);
		for(const domainData of domainDatas){
			await GoDaddyService.updateAuctionDetails(domainData);
		}
	}

	public static async trackPriceAll() {
		const domainToTracks = await DomainData.findAll({
			where: {
				timeEndDate: { [Op.between]: [
					moment().valueOf(), moment().add(24, 'hours').valueOf()
				] },
				[Op.or]: {
					bookContactId: { [Op.ne]: null },
					trackPrice: true,
				}
			}
		});

		for (const domain of domainToTracks){
			if (!domain.trackPrice) {
				domain.trackPrice = true;
				domain.save();
			}

			if (!this.runningTrackPrice.includes(domain.id)){
				this.runningTrackPrice.push(domain.id);
				this.trackPriceContinuous(domain)
			}
		}
	}
	public static async trackPriceContinuous(domain: DomainData) {
		if (!domain.bidUrl || !domain.bidUrl.includes('godaddy')) return;
		const { timeEndDate } = await GoDaddyService.getAuctionDetails(domain.bidUrl);


		if (timeEndDate && moment(timeEndDate).isAfter(moment())){
			const remainingMinutes = moment.duration( moment(timeEndDate).diff(moment()) ).asMinutes();
			const waitingTime = remainingMinutes < 5 ? 5 : remainingMinutes - 2;
			await System.sleep( waitingTime * 60 * 1000);
			this.trackPriceContinuous(domain);
		}
	}
}

