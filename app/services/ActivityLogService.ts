import { ActivityLog } from '../models/ActivityLog';

class ActivityLogService {
	static async Save(description: string): Promise<ActivityLog> {
		const result = await ActivityLog.create({ description });
		return result;
	}
}

export default ActivityLogService;
