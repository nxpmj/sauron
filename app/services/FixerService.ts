import { StringUtils } from '../core/util/StringUtils';
import { DomainData } from '../models/DomainData';
import { DataAhrefs } from '../models/DataAhrefs';
import GoDaddyService from './scraper/GoDaddyService';
import { System } from '../core';

export default class FixerService {
	static async detailsFixerById(idList: number[]){
		const domainDatas = await DomainData.findAll({
			where:{
				id: idList
			},
			include: [DataAhrefs]
		});
		for(const domainData of domainDatas){
			this.detailsFixerSingle(domainData);
			await System.sleep(5000);
		}
	}
	static async detailsFixerSingle(domainData: DomainData){
		await GoDaddyService.updateBidUrl(domainData)
		await GoDaddyService.updateAuctionDetails(domainData);

		if (domainData.vendorId){
			if(domainData.vendorId.includes('?')){
				domainData.vendorId = domainData.vendorId.split('?')[0];
			}
		}

		if (domainData.bidUrl){
			if(domainData.bidUrl.includes('?')){
				domainData.bidUrl = domainData.bidUrl.split('?')[0];
			}
			if(domainData.bidUrl.includes('godaddy') && domainData.bidUrl.includes('auctions')){
				if (domainData.vendorName == '-' || !domainData.vendorName){
					domainData.vendorName = 'GODADDY_AUCTIONS';
				}
				if(domainData.bidUrl.includes('-')){
					if (domainData.vendorId == '-' || !domainData.vendorId){
						domainData.vendorId = domainData.bidUrl.split('-').slice(-1)[0];
					}
				}
			}
		}
	}

}

