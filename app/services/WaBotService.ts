import { Env, System } from '../core';
import { AxiosService } from '../core/service/AxiosService';
import { RedisService } from '../core/service/RedisService';
import { StringUtils } from '../core/util/StringUtils';
import BookingService from './BookingService';
import WaService from './notification/WaService';
import ScraperService from './scraper/ScraperService';

export default class WaBotService {
	public static async initListener(){
		RedisService.subscribe({
			key: 'sauron'
		}, (messageObj) => {
			this.handleIncomingMessage(messageObj);
		});
	}
	private static async handleIncomingMessage(messageObj: any){
		// console.log('new message: '+JSON.stringify(messageObj));
		const simplifyMessage = messageObj.simplify;

		if (simplifyMessage.chatId != '120363040841567234@g.us') return;
		simplifyMessage.text = simplifyMessage.text.toLowerCase();
		const mentionedDomain = this.extractDomain(simplifyMessage.text);
		// const hasCancelKeyword = StringUtils.includesArray(simplifyMessage.text.toLowerCase(), ['cancel', 'ga jd', 'batal']);
		// const hasBookKeyword = StringUtils.includesArray(simplifyMessage.text.toLowerCase(), ['book', 'bid', 'ambil']);

		// if (simplifyMessage.reply && simplifyMessage.reply.isBot){
		if (simplifyMessage.text.toLowerCase() == 'show deadline all'){
			BookingService.incomingDeadline(messageObj, 'ALL');
		}
		else if (simplifyMessage.text.toLowerCase() == 'show deadline'){
			BookingService.incomingDeadline(messageObj, 'INCOMING');
		}
		else if (simplifyMessage.text.toLowerCase() == 'show deadline buynow 5k'){
			BookingService.incomingDeadline(messageObj, 'BUYNOW5K');
		}
		else if (simplifyMessage.text.toLowerCase() == 'show deadline buynow'){
			BookingService.incomingDeadline(messageObj, 'BUYNOW');
		}
		else if (simplifyMessage.reply){
			const quotedText = simplifyMessage.reply.quotedMessage.conversation;
			const quotedDomain = this.extractDomain(quotedText);
			if (quotedDomain && simplifyMessage.text.toLowerCase() == 'cancel'){
				await BookingService.confirmCancelBooking(messageObj, quotedDomain);
			}
			else if (quotedDomain  && simplifyMessage.text.toLowerCase() == 'book'){
				await BookingService.confirmBooking(messageObj, quotedDomain);
			}
		} else if (mentionedDomain){
			if (this.isStatsCommand(simplifyMessage.text)){
				await ScraperService.CheckStatsTask(mentionedDomain);
			}
			if (this.isUrlCommand(simplifyMessage.text)){
				await BookingService.infoUrl(messageObj, mentionedDomain);
			}
			else if (this.isCancelBookCommand(simplifyMessage.text)){
				await BookingService.confirmCancelBooking(messageObj, mentionedDomain);
			}
			else if (this.isBookCommand(simplifyMessage.text)){
				await BookingService.confirmBooking(messageObj, mentionedDomain);
			}
		}
	}

	private static extractDomain(text: string){
		text += '\n';
		for (const splitLines of text.split('\n')){
			for (let splitSentence of splitLines.split(' ')){
				if (splitSentence.split('.').length > 1) {
					splitSentence = splitSentence.replace('"', '');
					splitSentence = splitSentence.replace('"', '');
					splitSentence = splitSentence.replace('https://', '');
					splitSentence = splitSentence.replace('http://', '');
					return splitSentence;
				}
			}
		}
		return '';
	}

	private static isDomain(text: string): boolean{
		const textSplit = text.split('.');
		if (textSplit.length <= 1 || 5 <= textSplit.length) return false;
		return true;
	}

	private static isStatsCommand(text: string): boolean{
		return this.isDomainCommand('stats', text);
	}
	private static isUrlCommand(text: string): boolean{
		return this.isDomainCommand('url', text);
	}
	private static isBookCommand(text: string): boolean{
		return this.isDomainCommand('book', text);
	}
	private static isCancelBookCommand(text: string): boolean{
		return this.isDomainCommand('cancel', text);
	}

	private static isDomainCommand(firstWord: string, text: string): boolean{
		text = text.toLowerCase();
		const textSplit = text.split(' ');
		if (textSplit.length != 2) return false;
		if (textSplit[0] != firstWord) return false;
		if (!this.isDomain(textSplit[1])) return false;

		return true;
	}

}

