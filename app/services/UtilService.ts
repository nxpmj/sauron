import { StringUtils } from '../core/util/StringUtils';

class UtilService {
	static async Sleep(milliseconds: number) {
		return new Promise((resolve) => {
			setTimeout(resolve, milliseconds);
		});
	}

	// https://stackoverflow.com/a/1186465
	static FindGcd(a: number, b: number): number {
		if (b === 0) {
			return a;
		}
		return this.FindGcd(b, a % b);
	}

	static ConvertToSmallestRatio(a: number, b: number): string {
		if (isNaN(a) || isNaN(b)) {
			return 'N/A';
		}
		const gcd = this.FindGcd(a, b);
		return `${a / gcd}:${b / gcd}`;
	}
	static StringToInt(amountText: string): number {
		if (!amountText) return null;
		if (amountText == '-') return null;

		amountText = amountText.replace('$', '');
		amountText = amountText.replace(',', '');
		amountText = amountText.replace(',', '');
		amountText = amountText.replace('*', '');
		amountText = amountText.trim();

		const amount = parseFloat(amountText);

		return isNaN(amount) ? null : amount;
	}

	static normalizeNumberBatch(data: any, attributes: string[]){
		attributes.forEach(attribute => {
			data[attribute] = this.normalizeNumber(data[attribute]);
		});
	}
	static normalizeNumberAll(data: any){
		for(const key in data){
			if (typeof data[key] == 'string') data[key] = this.normalizeNumber(data[key]);
		}
	}
	static normalizeNumber(numberInString: string){
		if (!numberInString) return;
		if (numberInString == '-') return null;
		numberInString = StringUtils.trimChar(numberInString, ['$', ',', '%', '\n', '\t']);
		if (numberInString.includes('K')){
			return ( parseFloat(numberInString.split('K')[0]) * 1000 ).toString();
		}
		if (numberInString.includes('M')){
			return ( parseFloat(numberInString.split('M')[0]) * 1000000 ).toString();
		}
		return numberInString.trim();
	}
	static normalizeNumberInt(numberInString: string): number{
		if (!numberInString) return;

		const normalizeNumber = this.normalizeNumber(numberInString);
		if (!normalizeNumber) return;

		const numberInt = normalizeNumber.includes('.') ? parseFloat(normalizeNumber) : parseInt(normalizeNumber);
		return isNaN(numberInt) ? null : numberInt;
	}
}

export default UtilService;
