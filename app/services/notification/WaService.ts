import { Env, System } from '../../core';
import { AxiosService } from '../../core/service/AxiosService';
import { Contact } from '../../models/Contact';
import { RankService } from '../RankService';
import { Constant } from '../../common/constant/Constant';

export default class WaService {
	static defaultGroupName = 'Perguruan SEO';
	static confirmBookingMessage = `Successfully booked! reply "cancel" to Cancel booking`;

	public static async Send(message: string, quotedMessage?: any) {
		const data = {
			groupName: this.defaultGroupName,
			message,
			quotedMessage
		};

		const url = `${Env().wa.url}`;
		try {
			console.log('WaService message '+message);
			if (!Env().wa.url) return;
			console.log('WaService url '+url);
			await AxiosService.post(url, data);
		} catch (err) {
			console.error('WaService.send Error', err);
		}
	}

	public static async SendDomainData(data: Array<Record<string, any>>) {
		console.log('WaService SendDomainData '+data.length);
		for (const record of data){
			console.log('WaService loop start');
			const star = RankService.rankToStar(record.rankLOP);

			let message =  `${record.name} ${star}\n`;
			message += `Rank: ${record.rankLOP ?? '?'} DA: ${record.da} PA: ${record.pa} UR: ${record.ur}\n DR: ${record.dr}\n`;
			message += `TF: ${record.tf} Age: ${record.age} Traffic: ${record.totalTraffic} ($${record.trafficValue})\n`;
			message += `SS: ${record.ss} OrgKW: ${record.organicKeyword} OrgTraf:${record.organicTraffic}\n`;
			message += `Last Price: $${record.latestPrice ?? '???'}\n`;

			if (record.vendorName == Constant.VENDOR_NAME.GODADDY_BUY_NOW) message += `Buy Now ${record.buyNowPrice}`;
			else if (record.deadline) message += `${record.deadline}`;

			this.Send(message);
			await System.sleep(this.randomNumber(10, 30) * 1000);
			console.log('WaService loop end');
		}
	}
	static randomNumber(min, max) {
		return Math.random() * (max - min) + min;
	}


}

