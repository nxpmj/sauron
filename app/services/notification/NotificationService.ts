import * as moment from 'moment';
import { Env } from '../../core';
import { DomainData } from '../../models/DomainData';
import LoggerService from '../LoggerService';
import UtilService from '../UtilService';
import EmailService from './EmailService';
import WaService from './WaService';
import { DataAhrefs } from '../../models/DataAhrefs';

export default class NotificationService {
	static async SendNotification(domainDataList: Record<string, DomainData>, notifyEmail: boolean = true): Promise<void> {
		const domainDataNotification = [];
		for(const domainData of Object.values(domainDataList)){
			const dataAhrefs = await DataAhrefs.findOne({
				where: {domainDataId: domainData.id}
			})
			domainDataNotification.push( {
				name: domainData.name,
				da: domainData.da,
				pa: domainData.pa,
				ss: domainData.ss,
				age: parseFloat(domainData.age).toFixed(2),
				tf: domainData.tf,
				cf: domainData.cf,
				dr: dataAhrefs.dr ?? '-',
				ur: dataAhrefs.ur ?? '-',
				tfCfRatio: UtilService.ConvertToSmallestRatio(parseInt(domainData.tf), parseInt(domainData.cf)),
				alexaRank: domainData.alexaRank,
				organicKeyword: domainData.organicKeyword,
				organicTraffic: domainData.organicTraffic,
				bidCount: domainData.bidCount,
				estimatedValue: domainData.estimatedValue,
				latestPrice: domainData.latestPrice,
				latestPriceText: domainData.latestPriceText,
				timeLeft: domainData.timeLeft,
				bidUrl: domainData.bidUrl,
				deadline: moment(domainData.timeEndDate).format('dddd, DD MMM YYYY, HH:mm:ss'),
				totalTraffic: dataAhrefs?.totalTraffic ?? '?',
				trafficValue: dataAhrefs?.trafficValue ?? '?',
				vendorName: domainData.vendorName,
				buyNowPrice: domainData.buyNowPrice,
				rankLOP: domainData.rankLop,
			});
		}
		if (notifyEmail) await EmailService.SendDomainData(domainDataNotification);
		await LoggerService.LogActivity('Email delivered');

		WaService.SendDomainData(domainDataNotification);

		await LoggerService.LogActivity('End');
	}
}

