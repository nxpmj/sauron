import { AxiosService } from '../../core/service/AxiosService';
import { Env } from '../../core';
import * as moment from 'moment';

class EmailService {
	private static async Send(data: any) {
		const options = {
			api_key: Env().smtp2go.key,
			to: Env().email.domainDataTargets,
			sender: 'Sauron-bot <joseph.read@usa.com>',
			...data,
		};

		const url = `${Env().smtp2go.url}/email/send`;
		try {
			await AxiosService.post(url, options);
		} catch (err) {
			console.log('EmailService.send Error', err);
		}
	}

	static async SendDomainData(data: Record<string, any>[]) {
		let dataAsHtml = '<table border="1">';

		dataAsHtml += '</tr>';
		for (const key in data[0]) {
			dataAsHtml += `<td>${key}</td>`;
		}
		dataAsHtml += '</tr>';

		data.forEach((row) => {
			dataAsHtml += '<tr>';
			for (const key in row) {
				dataAsHtml += `<td>${row[key]}</td>`;
			}
			dataAsHtml += '</tr>';
		});

		await EmailService.Send({ subject: 'New Domains found', html_body: dataAsHtml });
	}

	static async SendNoDataFound() {
		await EmailService.Send({
			subject: 'No Domains found',
			to: ['rivaldi@ravencode.id'],
			text_body: `No data found on ${moment(new Date(), Env().timezone).format('dddd, DD MMM YYYY, HH:mm:ss')}`,
		});
	}

	static async SendErrorDetails(error: Error) {
		await EmailService.Send({
			subject: 'Sauron encountered error',
			to: ['rivaldi@ravencode.id'],
			text_body: error.toString(),
		});
	}
}

export default EmailService;
