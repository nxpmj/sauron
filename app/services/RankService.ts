import * as checkWord  from 'check-if-word';
import { DataAhrefs } from '../models/DataAhrefs';
import { DataAhrefsTraffic } from '../models/DataAhrefsTraffic';
import { DataAhrefsTrafficDetails } from '../models/DataAhrefsTrafficDetails';
import { DomainData } from '../models/DomainData';
import DomainDataService from './DomainDataService';
import LoggerService from './LoggerService';
const words = checkWord('en');

export class RankService {
	static async addRankLopV1(domainDataList: Record<string, DomainData>): Promise<void> {
		for(const domainData of Object.values(domainDataList)){
			const dataAhrefs = await DataAhrefs.findOne({
				where: {
					domainDataId: domainData.id
				},
				include: [{
					model: DataAhrefsTraffic,
					include: [ DataAhrefsTrafficDetails ]
				}]
			});

			const topTrafficList = this.getTopTraffic(dataAhrefs);

			let rankValue = 0;

			// CHECK Domain Type
			const domainType = domainData.name.split('.')[1];
			if (domainType == 'org') rankValue += 0.5;
			if (domainType == 'edu') rankValue += 0.5;

			// CHECK DA PA
			const dapa = parseInt(domainData.da) + parseInt(domainData.pa);
			if (dapa <= 55) rankValue += -1;
			else if (dapa >= 80) {
				rankValue += Math.floor( (dapa - 60)/20 );
			}

			// CHECK SS
			if (parseInt(domainData.ss) > 5) rankValue += -1;

			// CHECK AGE
			if (domainData.age != '-' && parseInt(domainData.age) < 10) rankValue += -1;

			if (dataAhrefs){
				// CHECK DR
				if (dataAhrefs.dr > 40) rankValue += 1;

				// CHECK TrafficValue
				// if (dataAhrefs.trafficValue > 10) rankValue++;
				if (dataAhrefs.trafficValue > 100) rankValue++;
				//if (dataAhrefs.trafficValue > 200) rankValue++;
				if (dataAhrefs.trafficValue > 1000) rankValue++;
				if (dataAhrefs.trafficValue > 10000) rankValue++;
				if (dataAhrefs.trafficValue > 100000) rankValue++;
				//console.log(`trafficValue ${dataAhrefs.trafficValue}`);

				// CHECK totalTraffic
				if (dataAhrefs.totalTraffic > 100) rankValue++;
				if (dataAhrefs.totalTraffic > 500) rankValue++;
				if (dataAhrefs.totalTraffic > 1000) rankValue++;
				// if (dataAhrefs.totalTraffic > 5000) rankValue++;
				if (dataAhrefs.totalTraffic > 10000) rankValue++;
				if (dataAhrefs.totalTraffic > 100000) rankValue++;
				//console.log(`totalTraffic ${dataAhrefs.totalTraffic}`);
				let countTop5 = 0;
				let countTop10 = 0;
				let countTop10StrongKeyword = 0;
				let countTop10SuperStrongKeyword = 0;
				let countTop20 = 0;
				let countTop20EnglishPartial = 0;
				let countTop20ValueMinimum = 0;
				let countTop20ValueMedium = 0;
				let countTop20ValueHigh = 0;
				dataAhrefs.dataAhrefsTraffics.forEach( dataAhrefsTraffic => {
					if (dataAhrefsTraffic.topKeywordsPosition <= 5 && this.isEnglishKeyword(dataAhrefsTraffic.topKeywords)) countTop5++;
					if (dataAhrefsTraffic.topKeywordsPosition <= 10 && this.isEnglishKeyword(dataAhrefsTraffic.topKeywords)) {
						countTop10++;
						if (dataAhrefsTraffic.topKeywordsVolume > 100000) countTop10SuperStrongKeyword++;
						if (dataAhrefsTraffic.topKeywordsVolume > 10000) countTop10StrongKeyword++;
					}
					if (dataAhrefsTraffic.topKeywordsPosition <= 20 && this.isEnglishKeyword(dataAhrefsTraffic.topKeywords)) {
						countTop20++;
						if (dataAhrefsTraffic.value > 100) countTop20ValueHigh++;
						if (dataAhrefsTraffic.value > 20) countTop20ValueMedium++;
						if (dataAhrefsTraffic.value > 0) countTop20ValueMinimum++;
					}
					if (dataAhrefsTraffic.topKeywordsPosition <= 20 && this.isPartialEnglishKeyword(dataAhrefsTraffic.topKeywords)) {
						countTop20EnglishPartial++;
					}
				})
				if (countTop10 >= 20) rankValue++;
				if (countTop20 >= 20 || countTop20EnglishPartial >= 40) rankValue++;
				if (countTop10StrongKeyword >= 5) rankValue++;
				if (countTop10SuperStrongKeyword >= 1) rankValue++;
				if (countTop20ValueMinimum >= 20) rankValue++;
				if (countTop20ValueMedium >= 10) rankValue++;
				if (countTop20ValueHigh >= 1) rankValue++;
				/*console.log(`countTop5 ${countTop5}`);
				console.log(`countTop10 ${countTop10}`);
				console.log(`countTop10StrongKeyword ${countTop10StrongKeyword}`);
				console.log(`countTop10SuperStrongKeyword ${countTop10SuperStrongKeyword}`);
				console.log(`countTop20 ${countTop20}`);
				console.log(`countTop20ValueMinimum ${countTop20ValueMinimum}`);
				console.log(`countTop20ValueMedium ${countTop20ValueMedium}`);
				console.log(`countTop20ValueHigh ${countTop20ValueHigh}`);*/

				// check link with strong keywords 10k++ & top10
				// check link with super strong keywords 100k++ & top10
			}


			const {unique, uniqueEnglish} = this.countUniqueKeywords(topTrafficList);
			if (unique > 10) rankValue++;
			if (unique > 50) rankValue++;
			if (unique > 100) rankValue++;
			if ((uniqueEnglish / unique) < 0.5) rankValue--;
			if ((uniqueEnglish / unique) < 0.2) rankValue--;
			//console.log(`uniqueKeywords ${unique}`);
			//console.log(`uniqueKeywordsEnglish ${uniqueEnglish}`);

			let rankString = 'F';
			if(rankValue >= 10) rankString = 'SSS';
			else if(rankValue >= 9) rankString = 'SS';
			else if(rankValue >= 8) rankString = 'S';
			else if(rankValue >= 7) rankString = 'A+';
			else if(rankValue >= 6) rankString = 'A';
			else if(rankValue >= 5) rankString = 'B+';
			else if(rankValue >= 4) rankString = 'B';
			else if(rankValue >= 3) rankString = 'C';
			else if(rankValue >= 2) rankString = 'D';
			else if(rankValue >= 1) rankString = 'E';

			domainData.rankLop = rankString;
			if (!domainData.bookContactId && domainData.rankLop != 'E' && domainData.rankLop != 'F' && domainData.timeEndDate) domainData.bookContactId = 4;
			// if (rankValue >= 4)
			console.log(`${domainData.name}: ${rankString} ${rankValue}`);
		}
	}
	static async addRankLop(domainDataList: Record<string, DomainData>): Promise<void> {
		for(const domainData of Object.values(domainDataList)){
			const dataAhrefs = await DataAhrefs.findOne({
				where: {
					domainDataId: domainData.id
				},
				include: [{
					model: DataAhrefsTraffic,
					include: [ DataAhrefsTrafficDetails ]
				}]
			});

			let rankValue = 0;

			// CHECK Domain Type
			const domainType = domainData.name.split('.')[1];
			if (domainType == 'org') rankValue += 0.5;
			if (domainType == 'edu') rankValue += 0.5;

			// CHECK DA PA
			const da = parseInt(domainData.da);
			const pa = parseInt(domainData.pa);
			const dapa = da + pa;
			if (dapa >= 60) rankValue += Math.floor( (dapa - 50)/10 );
			if (da < 10) rankValue -= 1;
			else if (da < 20) rankValue -= 0.5;
			if (pa < 10) rankValue -= 1;
			else if (pa < 20) rankValue -= 0.5;

			// CHECK SS
			if (parseInt(domainData.ss) > 30) rankValue += -1;

			// CHECK AGE
			if (domainData.age != '-' && parseInt(domainData.age) < 5) rankValue += -1;
			else if (domainData.age != '-' && parseInt(domainData.age) < 10) rankValue += -0.5;

			if (dataAhrefs){
				// CHECK DR
				const dr = dataAhrefs.dr;
				const ur = dataAhrefs.ur;
				const drur = dr + ur;
				if (drur >= 60) rankValue += Math.floor( (drur - 50)/10 );

				if (dr < 10) rankValue -= 1;
				else if (dr < 20) rankValue -= 0.5;
				if (ur < 10) rankValue -= 0.5;

				// CHECK TrafficValue
				// if (dataAhrefs.trafficValue > 10) rankValue++;
				if (dataAhrefs.trafficValue > 300) rankValue += 0.5;
				if (dataAhrefs.trafficValue > 1000) rankValue += 0.5;
				if (dataAhrefs.trafficValue > 10000) rankValue++;
				if (dataAhrefs.trafficValue > 100000) rankValue++;
				//console.log(`trafficValue ${dataAhrefs.trafficValue}`);

				// CHECK totalTraffic
				if (dataAhrefs.totalTraffic > 300) rankValue += 0.5;
				if (dataAhrefs.totalTraffic > 1000) rankValue += 0.5;
				if (dataAhrefs.totalTraffic > 10000) rankValue++;
				if (dataAhrefs.totalTraffic > 100000) rankValue++;
				//console.log(`totalTraffic ${dataAhrefs.totalTraffic}`);
			}


			const topTrafficList = this.getTopTraffic(dataAhrefs);
			const {unique, uniqueEnglish} = this.countUniqueKeywords(topTrafficList);
			if ((uniqueEnglish / unique) < 0.2) rankValue -= 1;
			//console.log(`uniqueKeywords ${unique}`);
			//console.log(`uniqueKeywordsEnglish ${uniqueEnglish}`);

			let rankString = 'F';
			if(rankValue >= 10) rankString = 'SSS';
			else if(rankValue >= 9) rankString = 'SS';
			else if(rankValue >= 8) rankString = 'S';
			else if(rankValue >= 7) rankString = 'A+';
			else if(rankValue >= 6) rankString = 'A';
			else if(rankValue >= 5) rankString = 'B+';
			else if(rankValue >= 4) rankString = 'B';
			else if(rankValue >= 3) rankString = 'C';
			else if(rankValue >= 2) rankString = 'D';
			else if(rankValue >= 1) rankString = 'E';

			domainData.rankLop = rankString;
			if (!domainData.bookContactId && domainData.rankLop != 'E' && domainData.rankLop != 'F' && domainData.timeEndDate) domainData.bookContactId = 4;
			// if (rankValue >= 4)
			console.log(`${domainData.name}: ${rankString} ${rankValue}`);
		}
	}
	static async filterRankLop(domainDataList: Record<string, DomainData>): Promise<Record<string, DomainData>> {
		const filteredDomainDataList: Record<string, DomainData> = {};
		for(const domainData of Object.values(domainDataList)){
			if (domainData.rankLop == 'F' || domainData.rankLop == 'E' || domainData.rankLop == 'D') continue;

			filteredDomainDataList[domainData.name] = domainData;
		}
		return filteredDomainDataList;
	}
	static getTopTraffic(dataAhrefs: DataAhrefs): any[] {
		if (!dataAhrefs) return [];
		const normalizeList = [];
		for(const traffic of dataAhrefs.dataAhrefsTraffics){
			if (!traffic.dataAhrefsTrafficDetails || traffic.dataAhrefsTrafficDetails.length == 0){
				if (traffic.topKeywordsPosition > 20) continue;
				normalizeList.push({
					traffic: traffic.traffic,
					value: traffic.value,
					keyword: traffic.topKeywords,
					volume: traffic.topKeywordsVolume,
					position: traffic.topKeywordsPosition,
				});
			} else {
				for(const trafficDetails of traffic.dataAhrefsTrafficDetails){
					if (trafficDetails.position > 20) continue;
					normalizeList.push({
						traffic: trafficDetails.traffic,
						value: 0,
						keyword: trafficDetails.keyword,
						volume: trafficDetails.volume,
						position: trafficDetails.position,
					});
				}

			}
		}
		return normalizeList;
	}
	static countUniqueKeywords(trafficList: any[]): {
		unique: number,
		uniqueEnglish: number
	} {
		const keywords = [];
		for (const item of trafficList){
			if (!keywords.includes(item.keyword)) keywords.push(item.keyword);
		}

		// console.log(keywords);
		let uniqueEnglish = 0;
		for (const keyword of keywords){
			if (this.isEnglishKeyword(keyword)) uniqueEnglish++;
		}
		return {
			unique: keywords.length,
			uniqueEnglish
		};
	}
	static isEnglishKeyword(keyword: string): boolean{
		const keywordTokenize = keyword.split(' ');
		let isValidEnglish = true;
		for (const keywordToken of keywordTokenize){
			if ( !words.check(keywordToken) ) isValidEnglish = false;
		}
		return isValidEnglish;
	}
	static isPartialEnglishKeyword(keyword: string): boolean{
		const keywordTokenize = keyword.split(' ');
		let isValidEnglish = false;
		for (const keywordToken of keywordTokenize){
			if ( words.check(keywordToken) ) isValidEnglish = true;
		}
		return isValidEnglish;
	}

	static rankToStar(rankLop: string){
		let star = '';
		if (rankLop == 'A') star = '⭐';
		if (rankLop == 'A+') star = '🌟';
		if (rankLop == 'S') star = '🌟🌟';
		if (rankLop == 'SS') star = '🌟🌟🌟';
		if (rankLop == 'SSS') star = '🌟🌟🌟🌟';
		return star;
	}
}
