import { Op } from 'sequelize';
import * as moment from 'moment';
import { DomainData } from '../models/DomainData';
import { DomainDataAttributes } from '../types/DomainData';
import { Env } from '../core';
import { DataAhrefs } from '../models/DataAhrefs';

export default class DomainDataService {
	static async FindAll(name: string[]): Promise<DomainData[]> {
		return await DomainData.findAll({
			where: {
				name: {[Op.in]: name},
				createdAt: { [Op.gte]: moment().subtract(30, 'days').toDate() },
			},
			include: [DataAhrefs]
		});
	}

	static async bulkSave(domainDataList: Record<string, DomainData>): Promise<void> {
		for(const domainData of Object.values(domainDataList)){
			await domainData.save();
		}
	}
	static async updateTimeEndData(domainData: DomainData): Promise<void> {
		if (domainData.timeLeft == '-') return;
		domainData.timeEndDate = this.timeLeftToDate(domainData.timeLeft, moment(domainData.createdAt)).toDate();
		await domainData.save();
	}
	static timeLeftToDate(timeLeft: string, timeFrom: moment.Moment = moment()): moment.Moment {
		if (timeLeft == '-') return null;

		let deadline = moment(timeFrom, Env.timezone);
		const timeLeftSplit = timeLeft.split(' ');
		for (const time of timeLeftSplit) {
			const period = time.slice(-1).toLowerCase();
			const duration = parseInt(time.substring(0, time.length - 1));
			deadline = deadline.add(
				duration as moment.DurationInputArg1,
				period as moment.unitOfTime.DurationConstructor
			);
		}
		return deadline;
	}

	static dateToTimeLeft(timeEnd: string): string {
		const end = moment(timeEnd, 'MM/DD/YYYY')
		const now = moment(new Date());

		const duration = moment.duration(end.diff(now));
		const minutes = Math.floor(duration.asMinutes());
		const hours = Math.floor(minutes/60);
		const days = Math.floor(hours/24)
		if(days>0){
			return (String(days)+'D ')+String(hours)+'H';
		}else{
			return (String(hours)+'H ')+String(minutes)+'M';
		}
	}
}
