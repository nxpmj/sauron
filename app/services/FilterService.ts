import { DomainData } from '../models/DomainData';
import LoggerService from './LoggerService';
import DomainDataService from './DomainDataService';
import { SpamZillaDomainFilteredData } from '../types/Scraper';
import { Constant } from '../common/constant/Constant';

export class FilterService {
	static async FilterDaPa(domainDataList: Record<string, DomainData>): Promise<Record<string, DomainData>> {
		const goodResults: Record<string, DomainData> = {};
		const badResults: Record<string, DomainData> = {};

		for (const domainName of Object.keys(domainDataList)) {
			const isGood1 = parseInt(domainDataList[domainName].da) >= 20 &&
				parseInt(domainDataList[domainName].pa) >= 20 &&
				parseInt(domainDataList[domainName].ss) < 50 &&
				(parseInt(domainDataList[domainName].age) >= 5 || !domainDataList[domainName].age) &&
				parseInt(domainDataList[domainName].tf) >= 5;
			const isGood2 = (parseInt(domainDataList[domainName].da) + parseInt(domainDataList[domainName].pa)) >= 60 &&
				parseInt(domainDataList[domainName].ss) < 30;

			if (isGood1 || isGood2 || domainDataList[domainName].forceCheck) {
				goodResults[domainName] = domainDataList[domainName];
			} else {
				badResults[domainName] = domainDataList[domainName];
			}
		}

		await LoggerService.LogActivity(
			`Filter good-bad da-pa results: ${Object.keys(goodResults).length}-${Object.keys(badResults).length}`
		);
		await DomainDataService.bulkSave(badResults);
		await LoggerService.LogActivity('Filter out bad da-pa results saved');
		return goodResults;
	}

	static async FilterDaPaOptional(domainDataList: Record<string, DomainData>): Promise<Record<string, DomainData>> {
		const goodResults: Record<string, DomainData> = {};
		const badResults: Record<string, DomainData> = {};

		for (const domainName of Object.keys(domainDataList)) {
			const isBad = (domainDataList[domainName].da != '' && parseInt(domainDataList[domainName].da) < 25) ||
				(domainDataList[domainName].da != '' && parseInt(domainDataList[domainName].pa) < 25) ||
				(domainDataList[domainName].ss != '' && parseInt(domainDataList[domainName].ss) > 50) ||
				(domainDataList[domainName].age != '' && parseInt(domainDataList[domainName].age) < 5) ||
				(domainDataList[domainName].tf != '' && parseInt(domainDataList[domainName].tf) < 10);
			const isGood = (parseInt(domainDataList[domainName].da) + parseInt(domainDataList[domainName].pa)) >= 60 &&
				parseInt(domainDataList[domainName].ss) < 30;
			if (isGood || domainDataList[domainName].forceCheck) {
				goodResults[domainName] = domainDataList[domainName];
			} else if (isBad) {
				badResults[domainName] = domainDataList[domainName];
			} else {
				goodResults[domainName] = domainDataList[domainName];
			}
		}

		await LoggerService.LogActivity(
			`Filter good-bad da-pa results: ${Object.keys(goodResults).length}-${Object.keys(badResults).length}`
		);
		await DomainDataService.bulkSave(badResults);
		await LoggerService.LogActivity('Filter out bad da-pa results saved');
		return goodResults;
	}

	static async filterOutExistingData(domainDataList: Record<string, DomainData>): Promise<Record<string, DomainData>> {
		await LoggerService.LogActivity(`Filter out existing data - count ${Object.values(domainDataList).length}`);
		const filteredDomainData: Record<string, DomainData> = {};

		const dataFromDb = await DomainDataService.FindAll(Object.keys(domainDataList));
		const recordDataFromDb: Record<string, DomainData> = {};
		for (const item of dataFromDb) {
			recordDataFromDb[item.name] = item;
		}


		for (const domainName in domainDataList) {
			if (domainDataList[domainName].vendorName == Constant.VENDOR_NAME.GODADDY_CLOSEOUTS) domainDataList[domainName].vendorName = Constant.VENDOR_NAME.GODADDY_AUCTIONS;

			const domainDataFromDB = recordDataFromDb[domainName];
			if (domainDataFromDB){
				const isSupposedToCheck = !domainDataFromDB.dataAhrefs && domainDataList[domainName].forceCheck && !domainDataFromDB.forceCheck;
				const isSameVendorId = !domainDataFromDB.vendorId || !domainDataList[domainName].vendorId || domainDataFromDB.vendorId == domainDataList[domainName].vendorId;
				const isSameVendorName = domainDataFromDB.vendorName == domainDataList[domainName].vendorName;

				if (isSupposedToCheck){
					domainDataFromDB.forceCheck = false;
					domainDataList[domainName] = domainDataFromDB;
				}
				else if (isSameVendorId && isSameVendorName) {
					continue;
				}
			}

			await domainDataList[domainName].save();
			filteredDomainData[domainName] = domainDataList[domainName];
		}

		await LoggerService.LogActivity(`Filter out existing data completed - count ${Object.values(filteredDomainData).length}`);
		return filteredDomainData;
	}

	static async FilterOrganicData(domainDataList: Record<string, DomainData>): Promise<Record<string, DomainData>> {
		const goodResults: Record<string, DomainData> = {};
		const badResults: Record<string, DomainData> = {};

		for (const domainName of Object.keys(domainDataList)) {
			const isGoodOrganicData = this.isGoodOrganicData(domainDataList[domainName].organicKeyword, domainDataList[domainName].organicTraffic);
			if (isGoodOrganicData) {
				goodResults[domainName] = domainDataList[domainName];
			} else {
				badResults[domainName] = domainDataList[domainName];
			}
		}

		await LoggerService.LogActivity(
			`Filter good-bad organic results: ${Object.keys(goodResults).length}-${Object.keys(badResults).length}`
		);
		await DomainDataService.bulkSave(badResults);
		await LoggerService.LogActivity('Filter out bad organic results saved');
		return goodResults;
	}

	static isGoodOrganicData(organicKeyword, organicTraffic) {
		if (!organicKeyword) organicKeyword = '0';
		if (!organicTraffic) organicTraffic = '0';
		const isOrganicKeywordPassing =
			parseInt(organicKeyword) >= 20 ||
			(!parseInt(organicKeyword.slice(-1)) && organicKeyword.slice(-1) !== '0');
		const isOrganicTrafficPassing =
			parseInt(organicTraffic) > 0 ||
			(!parseInt(organicTraffic.slice(-1)) && organicTraffic.slice(-1) !== '0');

		return isOrganicKeywordPassing && isOrganicTrafficPassing;
	}
}
