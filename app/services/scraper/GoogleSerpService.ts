import { GoogleSerpDomainData } from '../../types/Scraper';
import LoggerService from '../LoggerService';
import BaseScraperService from './BaseScraperService';
import { DomainData } from '../../models/DomainData';

/**
 * Documentation
 * https://apify.com/apify/google-search-scraper
 */
class GoogleSerpService extends BaseScraperService {
	static async ScrapDomainData(domainDataList: Record<string, DomainData>): Promise<void> {
		await LoggerService.LogActivity('Scraping Google SERP');
		const apifyClient = this.InitApifyClient();

		const results: Record<string, GoogleSerpDomainData> = {};

		for (const domainName of Object.keys(domainDataList)) {
			try {
				await LoggerService.LogActivity(`Google SERP scraping data for ${domainName}`);
				const params = {
					queries: `site:${domainName}`,
					resultsPerPage: 10,
					maxPagesPerQuery: 1,
					mobileResults: false,
					saveHtml: false,
					saveHtmlToKeyValueStore: false,
					includeUnfilteredResults: false,
					countryCode: 'id',
					locationUule: 'w+CAIQICIJaW5kb25lc2lh',
				};

				const callResult = await apifyClient.actor('apify/google-search-scraper').call(params);
				const datasetResult = await apifyClient.dataset(callResult.defaultDatasetId).listItems();
				domainDataList[domainName].searchCount = datasetResult.items[0].resultsTotal as string;
			} catch (err) {
				await LoggerService.LogActivity(`Error Google SERP for ${domainName}`, err);
			}
		}

		await LoggerService.LogActivity('Google SERP scraping completed');
	}
}

export default GoogleSerpService;
