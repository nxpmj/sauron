import * as moment from 'moment';
import * as puppeteer from 'puppeteer';
import { Env, System } from '../../core';
import { StringUtils } from '../../core/util/StringUtils';
import { DataAhrefs } from '../../models/DataAhrefs';
import { DataAhrefsTraffic } from '../../models/DataAhrefsTraffic';
import { DataAhrefsTrafficDetails } from '../../models/DataAhrefsTrafficDetails';
import { DomainData } from '../../models/DomainData';
import { AhrefsDomainData, SpamZillaDomainData } from '../../types/Scraper';
import LoggerService from '../LoggerService';
import UtilService from '../UtilService';
import BaseScraperService from './BaseScraperService';
import { FilterService } from '../FilterService';

class AhrefsService extends BaseScraperService {
	static async updateAhrefOrganicData(domainDataList: Record<string, DomainData>): Promise<void> {
		const browser = await puppeteer.launch({
			args: ['--no-sandbox'],
		});
		const ahrefsDomainDatas: Record<string, AhrefsDomainData> = {};
		const page = await browser.newPage();
		page.setDefaultNavigationTimeout(0);
		try {
			const doLogin = async (puppeteerPage: puppeteer.Page) => {
				await puppeteerPage.goto('https://app.ahrefs.com/user/login', { waitUntil: 'networkidle0' });
				await puppeteerPage.type(
					'#root > div > div > div > div.css-shut2q-contentBox > div > div > div > div > form > div > div.css-1pgj03c-container > input',
					Env().ahrefs.email
				);
				await puppeteerPage.type(
					'#root > div > div > div > div.css-shut2q-contentBox > div > div > div > div > form > div > div > div > input[name="password"]',
					Env().ahrefs.password
				);
				await puppeteerPage.click(
					'#root > div > div > div > div.css-shut2q-contentBox > div > div > div > div > form > div > button'
				);
				await puppeteerPage.waitForNavigation({ waitUntil: 'networkidle0' });
			};

			const getDataMain = async (puppeteerPage: puppeteer.Page, domainName: string) => {
				let organicChartJson;
				puppeteerPage.on('response', async (response) => {
					if (response.status() != 200) return;
					if (response.url().includes('seGetChartDataPhpCompat')){
						organicChartJson = await response.json();
					}
				})
				await puppeteerPage.goto(`https://app.ahrefs.com/site-explorer/overview/v2/subdomains/live?target=${domainName}`, {
					waitUntil: 'networkidle0',
					timeout: 0,
				});
				const data = await puppeteerPage.evaluate(() => {

					return {
						ahrefsRank: (document.querySelector('#topAhrefsRank')?.textContent),
						ur: (document.querySelector('#UrlRatingContainer > span')?.textContent),
						dr: (document.querySelector('#DomainRatingContainer > span')?.textContent),
						backlinks: (document.querySelector('#numberOfRefPages > a')?.textContent),
						referringDomains: (document.querySelector('#numberOfRefDomains > a')?.textContent),
						organicKeyword: (document.querySelector('#numberOfOrganicKeywords')?.textContent || '-').split(' ')[0],
						organicTraffic: (document.querySelector('#numberOfOrganicTraffic > span')?.textContent || '-').split(' ')[0],
						trafficValue: (document.querySelector('#numberOfOrganicTrafficCost')?.textContent),
					};
				});
				UtilService.normalizeNumberBatch(data, ['ahrefsRank', 'ur', 'dr', 'backlinks', 'referringDomains', 'organicKeyword', 'organicTraffic', 'trafficValue'])
				if (data.organicKeyword === '-' || data.organicTraffic === '-') {
					await this.TakeScreenshot(
						puppeteerPage,
						`${moment().format('YYYYMMDDHHmmss')}-ahrefs-data-not-found-${domainName}.png`
					);
				}
				data['organicChart'] = organicChartJson[1] ?? [];
				return data;
			};

			const getDataTraffics = async (puppeteerPage: puppeteer.Page, domainName: string) => {
				const trafficMain : any = {
					totalTraffic: 0,
					traffics: []
				};
				let hasNextPage = true;
				let pageIndex = 1;
				while(hasNextPage){
					const trafficPaginate = await getDataTrafficsPaginate(puppeteerPage, domainName, pageIndex);
					hasNextPage = trafficPaginate.hasNextPage;
					if (pageIndex == 1) trafficMain.totalTraffic = trafficPaginate.totalTraffic;
					trafficMain.traffics = [
						...trafficMain.traffics,
						...trafficPaginate.traffics
					]
					pageIndex++;
				}

				return trafficMain;
			};
			const getDataTrafficsPaginate = async (puppeteerPage: puppeteer.Page, domainName: string, pageIndex: number) => {
				await puppeteerPage.goto(`https://app.ahrefs.com/positions-explorer/top-pages/v2/subdomains/all/all/all/all/all/all/all/all/${pageIndex}/traffic_value_desc?target=${domainName}`, {
					waitUntil: 'networkidle0',
					timeout: 0,
				});
				const trafficMain = await puppeteerPage.evaluate(() => {
					const table = [];
					const trs = Array.from(document.querySelectorAll('#container > tr'));
					for (const tr of trs){
						const tds = Array.from( tr.querySelectorAll('td') );
						if (tds.length != 11) continue;

						const row: any = {
							traffic: (tds[1].querySelector('span')?.textContent).replace(',', '').replace(',', ''),
							value: (tds[3].textContent).replace('$', '').replace(',', ''),
							keywords: (tds[4].querySelector('a')?.textContent),
							rd: (tds[5].querySelector('div > p > a')?.textContent) ?? '0',
							url: (tds[6].querySelector('p > a')?.getAttribute('href')),
							topKeywords: (tds[7].querySelector('a')?.textContent),
							topKeywordsVolume: (tds[8].querySelector('p')?.textContent),
							topKeywordsPosition: (tds[10].querySelector('p')?.textContent),
						};
						// UtilService.normalizeNumberBatch(row, ['traffic', 'value', 'rd', 'url', 'topKeywords', 'topKeywordsVolume', 'topKeywordsPosition', 'trafficValue']);

						if (tds[3].querySelector('span')) row.value = 0;

						row.shouldExpand = parseInt(row.traffic) >= 100 || parseInt(row.value) >= 10;
						if (row.shouldExpand ) tds[4].querySelector('a').click();
						table.push(row);
					}
					const nextPrevButton = document.querySelector('[name="pager"] > .page-item > a.page-link.page.btn-ahrefs-sm');

					const trafficMainData = {
						totalTraffic: (document.querySelector('#result_info var[country-traffic]')?.textContent),
						traffics: table,
						hasNextPage: nextPrevButton && nextPrevButton?.textContent == 'Next'
					}

					return trafficMainData;
				});
				// await page.click("#myButton");
				await System.sleep(3000);
				const trafficDetails = await puppeteerPage.evaluate(() => {
					const innerTables = Array.from(document.querySelectorAll('[name="site_explorer_data_rows"].intable'));
					const tableData = [];
					try{
						for (const innerTable of innerTables){
							const table = innerTable.querySelector('table');
							if (!table) {
								tableData.push([]);
								continue;
							}

							const trs = Array.from(table.querySelectorAll('tbody > tr'));
							const details = [];
							for (const tr of trs){
								const tds = Array.from( tr.querySelectorAll('td') );
								const subRow: any = {
									keyword: tds[0].querySelector('a')?.textContent,
									position: tds[2].querySelector('span')?.textContent,
									volume: tds[3]?.textContent,
									kd: tds[4]?.textContent,
									cpc: tds[5]?.textContent,
									traffic: tds[6]?.textContent,
									results: tds[7]?.textContent,
									updateDate: tds[8]?.textContent,
									country: tds[9]?.textContent,
								};
								details.push(subRow);
							}
							tableData.push(details);
						}
					}catch(e){}
					return tableData;
				});

				let counter = 0;
				trafficMain.traffics.forEach( traffic => {
					traffic['trafficDetails'] = trafficDetails[counter];
					counter++;
				})

				return trafficMain;
			};

			await LoggerService.LogActivity('Loading Ahrefs');
			await doLogin(page);
			await LoggerService.LogActivity('Login success');

			const filteredDomainDataList = {};
			Object.values(domainDataList).forEach( domainData => {
				// DISABLE filter temp
				// if (domainData.organicKeyword == '-' ) filteredDomainDataList[domainData.name] = domainData
				filteredDomainDataList[domainData.name] = domainData
			});

			const groupedDomainList = this.SplitIntoGroups(Object.keys(filteredDomainDataList));
			await LoggerService.LogActivity('Scraping Ahrefs');
			await Promise.all(
				groupedDomainList.map(async (group) => {
					const pageForGroup = await browser.newPage();
					pageForGroup.setDefaultNavigationTimeout(0);

					for (const domainName of group) {
						await LoggerService.LogActivity(`Ahrefs scraping data for ${domainName}`);
						try {
							const domainData = filteredDomainDataList[domainName];

							await this.checkExGambling(browser, domainData);
							if (domainData.dataAhrefs && domainData.dataAhrefs.trafficSummary) continue;
							const mainData : any = await getDataMain(pageForGroup, domainName);

							let dataTraffic = { totalTraffic: 0, traffics: [] };
							if (FilterService.isGoodOrganicData(mainData.organicKeyword, mainData.organicTraffic) && parseInt(mainData.trafficValue ?? '0') > 0){
								if (!domainData.dataAhrefs){
									dataTraffic = await getDataTraffics(pageForGroup, domainName);
								}
							}
							ahrefsDomainDatas[domainName] = {
								organicKeyword: mainData.organicKeyword,
								organicTraffic: mainData.organicTraffic,
								dataAhref: {
									...mainData,
									...dataTraffic
								}
							}
							await UtilService.Sleep(2000);
						} catch (err) {
							await this.TakeScreenshot(
								pageForGroup,
								`./errors/${moment().format('YYYYMMDDHHmmss')}-ahrefs-error-${domainName}.png`
							);
							await LoggerService.LogActivity(`Ahrefs scraping error on ${domainName}`, err);
						}
					}
				})
			);
			await LoggerService.LogActivity('Ahrefs scraping completed');

			await browser.close();
			await this.updateToDomainData(domainDataList, ahrefsDomainDatas);
			await LoggerService.LogActivity('Ahrefs Update Data completed');
		} catch (err) {
			await this.TakeScreenshot(page, `./errors/${moment().format('YYYYMMDDHHmmss')}-ahrefs-error.png`);
			await LoggerService.LogActivity('Ahrefs scraping failed', err);
			await browser.close();
			throw err;
		}
		console.log();
	}

	private static async checkExGambling(browser: puppeteer.Browser, domain: DomainData){
		const dataAhrefs = domain.dataAhrefs;
		if (!dataAhrefs) return;
		if (dataAhrefs.isExGambling != null) return;

		const keywords = ['slot', 'judi', 'gambl'];
		const foundKeywords = [];

		for(const keyword of keywords){
			const page = await browser.newPage();

			let backlinkJson;
			page.on('response', async (response) => {
				if (response.status() != 200) return;
				if (response.url().includes('seBacklinks')){
					backlinkJson = await response.json();
				}
			})
			await page.goto(`https://app.ahrefs.com/v2-site-explorer/backlinks/subdomains?surroundingRules=%5B%5B%22contains%22%2C%22${keyword}%22%2C%22all%22%5D%5D&target=${domain.name}`, {
				waitUntil: 'networkidle0',
				timeout: 0,
			})
			await System.sleep(1000);

			const backlinkCount = backlinkJson ? (backlinkJson[1]?.count ?? 0) : 0;
			page.close();

			if (backlinkCount > 0) foundKeywords.push(keyword);
		}

		dataAhrefs.isExGambling = foundKeywords.length > 0;
		dataAhrefs.exGamblingKeywords = foundKeywords.join(',');
		await dataAhrefs.save();
	}

	private static async updateToDomainData(domainDataList: Record<string, DomainData>, ahrefsDomainDatas: Record<string, AhrefsDomainData>): Promise<void> {
		for (const domainName of Object.keys(ahrefsDomainDatas)) {
			const domainData = domainDataList[domainName];
			const ahrefsDomainData = ahrefsDomainDatas[domainName];
			domainData.organicKeyword = ahrefsDomainData.organicKeyword
			domainData.organicTraffic = ahrefsDomainData.organicTraffic

			let dataAhrefs;
			if (!domainData.dataAhrefs) {
				UtilService.normalizeNumberAll(ahrefsDomainData.dataAhref);
				dataAhrefs = await DataAhrefs.create({
					domainDataId: domainData.id,
					organicKeywords: ahrefsDomainData.organicKeyword,
					...ahrefsDomainData.dataAhref
				});

				for(const trafficData of ahrefsDomainData.dataAhref.traffics){
					UtilService.normalizeNumberAll(trafficData);
					const dataAhrefsTraffic = await DataAhrefsTraffic.create({
						domainDataId: domainData.id,
						dataAhrefsId: dataAhrefs.id,
						...trafficData
					});
					trafficData.trafficDetails.forEach( trafficDetailsItem => {
						trafficDetailsItem.domainDataId = domainData.id;
						trafficDetailsItem.dataAhrefsTrafficId = dataAhrefsTraffic.id;
						UtilService.normalizeNumberAll(trafficDetailsItem);
					})
					DataAhrefsTrafficDetails.bulkCreate(trafficData.trafficDetails);
				}
			} else {
				dataAhrefs = domainData.dataAhrefs;
			}

			const organicChart = {};
			ahrefsDomainData.dataAhref.organicChart.map((data) => organicChart[data.date] = data);

			const prevWeekToCheck = [0,1,2,3,4,8,12];
			dataAhrefs.trafficSummary = {}
			dataAhrefs.trafficValueSummary = {}
			dataAhrefs.keywordSummary = {}
			for(const prevWeek of prevWeekToCheck){
				const dateToCheck = moment().subtract(prevWeek, 'weeks').format('YYYY-MM-DD');
				dataAhrefs.trafficSummary[prevWeek] = organicChart[dateToCheck]?.organic.traffic ?? '';
				dataAhrefs.trafficValueSummary[prevWeek] = organicChart[dateToCheck]?.organic.cost ?? '';
				dataAhrefs.keywordSummary[prevWeek] = organicChart[dateToCheck]?.organic.position ?? '';
			}
			dataAhrefs.trafficSummary = JSON.stringify(dataAhrefs.trafficSummary);
			dataAhrefs.trafficValueSummary = JSON.stringify(dataAhrefs.trafficValueSummary);
			dataAhrefs.keywordSummary = JSON.stringify(dataAhrefs.keywordSummary);
			dataAhrefs.isDownward = dataAhrefs.trafficSummary[4] != 0 && (dataAhrefs.trafficSummary[4] * 0.80) > dataAhrefs.trafficSummary[0];
			dataAhrefs.save();


		}
	}

}

export default AhrefsService;
