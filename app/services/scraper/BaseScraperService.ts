import * as puppeteer from 'puppeteer';
import { Env } from '../../core';
import { ApifyClient } from 'apify-client';

class BaseScraperService {
	static apifyClient: ApifyClient | null = null;

	// https://stackoverflow.com/a/56011152
	static WaitForNetworkIdle(page: puppeteer.Page, timeout = 500, maxInflightRequests = 0) {
		page.on('request', onRequestStarted);
		page.on('requestfinished', onRequestFinished);
		page.on('requestfailed', onRequestFinished);

		let inflight = 0;
		let fulfill;
		let promise = new Promise((x) => (fulfill = x));
		let timeoutId = setTimeout(onTimeoutDone, timeout);
		return promise;

		function onTimeoutDone() {
			page.removeListener('request', onRequestStarted);
			page.removeListener('requestfinished', onRequestFinished);
			page.removeListener('requestfailed', onRequestFinished);
			fulfill();
		}

		function onRequestStarted() {
			++inflight;
			if (inflight > maxInflightRequests) clearTimeout(timeoutId);
		}

		function onRequestFinished() {
			if (inflight === 0) return;
			--inflight;
			if (inflight === maxInflightRequests) timeoutId = setTimeout(onTimeoutDone, timeout);
		}
	}

	static async TakeScreenshot(page: puppeteer.Page, path: string) {
		await page.screenshot({ path, fullPage: true });
	}

	static SplitIntoGroups(group: string[], groupCount = 3): string[][] {
		const grouper = [];
		for (let i = 0; i < groupCount; i++) {
			grouper.push([]);
		}

		let iterationCount = 0;
		group.forEach((member) => {
			iterationCount++;
			grouper[iterationCount - 1].push(member);

			if (iterationCount === groupCount) {
				iterationCount = 0;
			}
		});

		return grouper;
	}

	static InitApifyClient() {
		if (!this.apifyClient) {
			this.apifyClient = new ApifyClient({
				token: Env().apify.token,
			});
		}

		return this.apifyClient;
	}
}

export default BaseScraperService;
