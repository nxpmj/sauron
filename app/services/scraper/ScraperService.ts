import { DomainData } from '../../models/DomainData';
import { FilterService } from '../FilterService';
import LoggerService from '../LoggerService';
import NotificationService from '../notification/NotificationService';
import AhrefsService from './AhrefsService';
import GoDaddyService from './GoDaddyService';
import GoogleSerpService from './GoogleSerpService';
import SpamZillaService from './SpamZillaService';
import { WebSeoCheckerService } from './WebSeoCheckerService';
import EmailService from '../notification/EmailService';
import DomainDataService from '../DomainDataService';
import { AgeService } from './AgeService';
import { Op } from 'sequelize';
import * as moment from 'moment';
import { Env } from '../../core';
import { DataWebSeoChecker } from '../../models/DataWebSeoChecker';
import { RankService } from '../RankService';
import { DataAhrefs } from '../../models/DataAhrefs';
import ExpiredDomainService from './ExpiredDomainService';

class ScraperService {
	static async ScrapDomainDataTask() {
		try {
			await LoggerService.LogActivity('ScrapDomainDataJob Start');
			const domainDataList: Record<string, DomainData> = await this.ScrapDomainData();

			if (Object.keys(domainDataList).length === 0) {
				await EmailService.SendNoDataFound();
			} else {
				await NotificationService.SendNotification(domainDataList);
			}

			await LoggerService.LogActivity('ScrapDomainDataJob End');
		} catch (err) {
			console.error(err);
			await EmailService.SendErrorDetails(err);
		}
	}
	static async CheckStatsTask(domainDataName: string) {
		let domainData = await DomainData.findOne({
			where: {
				name: domainDataName
			},
			include: [DataAhrefs],
			order: [ ['id', 'DESC'] ]
		})

		if (!domainData){
			domainData = await DomainData.create({
				name: domainDataName
			});
			GoDaddyService.updateBidUrl(domainData);
		}
		let domainDataList: Record<string, DomainData> = {};
		domainDataList[domainData.name] = domainData;

		if (!domainData.dataAhrefs){
			await this.CheckStats(domainDataList, false);
		}

		if (!domainData.rankLop){
			await RankService.addRankLop(domainDataList);
			await DomainDataService.bulkSave(domainDataList);
		}


		if (domainDataList) await NotificationService.SendNotification(domainDataList, false);

	}
	static async ScrapDomainData(): Promise<Record<string, DomainData>> {

		let domainDataList: Record<string, DomainData> = await GoDaddyService.ScrapDomainData();
		domainDataList = await SpamZillaService.ScrapDomainListData(domainDataList);
		domainDataList = await ExpiredDomainService.ScrapDomainListData(domainDataList);


		domainDataList = await FilterService.filterOutExistingData(domainDataList);
		domainDataList = await FilterService.FilterDaPaOptional(domainDataList);

		if (Object.keys(domainDataList).length === 0) return {};
		domainDataList = await this.CheckStats(domainDataList);

		await LoggerService.LogActivity('Good results saved');
		return domainDataList;
	}
	static async CheckStats(domainDataList: Record<string, DomainData>, enableFilter = true): Promise<Record<string, DomainData>> {
		// await SpamZillaService.ScrapDomainData(domainDataList);

		await WebSeoCheckerService.updateMozData(domainDataList);
		if (enableFilter) domainDataList = await FilterService.FilterDaPaOptional(domainDataList);

		await AgeService.updateDomainAge(domainDataList);

		if (enableFilter) domainDataList = await FilterService.FilterDaPa(domainDataList);
		if (Object.keys(domainDataList).length === 0) return {};

		await AhrefsService.updateAhrefOrganicData(domainDataList);
		if (enableFilter) domainDataList = await FilterService.FilterOrganicData(domainDataList);
		if (Object.keys(domainDataList).length === 0) return {};

		// if (enableFilter) await GoogleSerpService.ScrapDomainData(domainDataList);
		await RankService.addRankLop(domainDataList);
		await GoDaddyService.updateBidUrlAll(domainDataList);

		await DomainDataService.bulkSave(domainDataList);
		if (enableFilter) domainDataList = await RankService.filterRankLop(domainDataList);

		return domainDataList;
	}
}

export default ScraperService;
