import * as puppeteer from 'puppeteer';
import { Env, System } from '../../core';
import { DomainData } from '../../models/DomainData';
import { GoDaddyDomainData } from '../../types/Scraper';
import DomainDataService from '../DomainDataService';
import LoggerService from '../LoggerService';
import UtilService from '../UtilService';
import BaseScraperService from './BaseScraperService';
import * as moment from 'moment';
import * as Util from 'util';
import { StringUtils } from '../../core/util/StringUtils';
import { Constant } from '../../common/constant/Constant';

class GoDaddyService extends BaseScraperService {
	static async ScrapDomainData(): Promise<Record<string, DomainData>> {
		const browser = await puppeteer.launch({
			args: ['--no-sandbox'],
		});
		const goDaddyDomainData: Record<string, DomainData> = {};
		try {
			const doLogin = async (puppeteerPage: puppeteer.Page) => {
				await puppeteerPage.goto('https://sso.godaddy.com/v1/login', { waitUntil: 'networkidle0' });

				const isLoginFormA = await puppeteerPage.evaluate(() => document.querySelector('#password'));
				if (isLoginFormA) {
					await LoggerService.LogActivity('Login Form A');
					await puppeteerPage.type('#username', Env().godaddy.email);
					await puppeteerPage.type('#password', Env().godaddy.password);
					await puppeteerPage.click('#submitBtn');
					await this.WaitForNetworkIdle(puppeteerPage);
				} else {
					await LoggerService.LogActivity('Login Form B');
					await puppeteerPage.type('#username', Env().godaddy.email);
					await puppeteerPage.click('#submitBtn');
					await Promise.all([this.WaitForNetworkIdle(puppeteerPage), puppeteerPage.waitForSelector('#password')]);

					await puppeteerPage.type('#password', Env().godaddy.password);
					await puppeteerPage.click('#submitBtn');
					await this.WaitForNetworkIdle(puppeteerPage);
				}
			};

			const waitSpinner = async (puppeteerPage: puppeteer.Page) => {
				await Promise.all([
					puppeteerPage.waitForSelector('#divOverlaySRA', { hidden: true }),
					puppeteerPage.waitForSelector('#divOverlaySRB', { hidden: true }),
				]);
			};

			const setRowsShownTo500 = async (puppeteerPage: puppeteer.Page) => {
				await puppeteerPage.select('#ddlRowsToReturn', '7');
				await this.WaitForNetworkIdle(puppeteerPage);
				await waitSpinner(puppeteerPage);
			};

			const getCurrentRows = async (puppeteerPage: puppeteer.Page) => {
				const [oddRows, evenRows] = await Promise.all([
					puppeteerPage.$x('//*[@id="search-table"]/tbody/tr[@class="srRow1"]'),
					puppeteerPage.$x('//*[@id="search-table"]/tbody/tr[@class="srRow2"]'),
				]);
				for (const row of [...oddRows, ...evenRows]) {
					const hasDomainName = !!(await row.evaluate((data) => data.querySelector('td > span.OneLinkNoTx')));
					if (hasDomainName) {
						const data = await row.evaluate((rowData) => {
							const elementId = rowData.getAttribute('id');
							const splitElementId = elementId.split('_');
							const id = splitElementId[splitElementId.length - 1];
							const domainName = rowData.querySelector('td > span.OneLinkNoTx').textContent.trim().toLowerCase();
							const bidCount = rowData.querySelector('td:nth-child(5) > a')?.textContent.trim() || '-';
							const estimatedValue = rowData.querySelector('td:nth-child(7) > span')?.textContent.trim() || '-';
							const latestPriceText = rowData.querySelector('td:nth-child(8)')?.textContent.trim() || '-';
							const timeLeft = rowData.querySelector('td:nth-child(10)')?.textContent.trim() || '-';
							const bidUrl = `https://sg.godaddy.com/domain-auctions/${domainName.split('.').join('-')}-${id}`;
							return {
								id,
								domainName,
								bidCount,
								estimatedValue,
								latestPriceText,
								timeLeft,
								bidUrl,
							};
						});

						goDaddyDomainData[data.domainName] = DomainData.build({
							name: data.domainName,
							bidCount: data.bidCount,
							estimatedValue: data.estimatedValue,
							latestPrice: UtilService.StringToInt(data.latestPriceText),
							latestPriceText:data.latestPriceText,
							timeLeft: data.timeLeft,
							timeEndDate: DomainDataService.timeLeftToDate(data.timeLeft),
							bidUrl: data.bidUrl,
							da: '-',
							pa: '-',
							ss: '-',
							age: '-',
							tf: '-',
							cf: '-',
							alexaRank: '-',
							organicKeyword: '-',
							organicTraffic: '-',
							searchCount: '-',
							vendorId: data.id,
							vendorName: 'GODADDY_AUCTIONS',
							aggregator: Constant.AGGREGATOR.GODADDY_AUCTIONS
						});
					}
				}
			};

			const scrapPage = async (puppeteerPage: puppeteer.Page, scrapNextPage = true) => {
				await getCurrentRows(puppeteerPage);
				const btnNextPage = await puppeteerPage.evaluate(() =>
					document.querySelector(
						'#tblSearchResults > table > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr > td:nth-child(4) > a'
					)
				);

				if (btnNextPage && scrapNextPage) {
					await LoggerService.LogActivity('Has next page');
					await UtilService.Sleep(60000);
					await LoggerService.LogActivity('Load next page');
					await puppeteerPage.click(
						'#tblSearchResults > table > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr > td:nth-child(4) > a'
					);
					await this.WaitForNetworkIdle(puppeteerPage);
					await waitSpinner(puppeteerPage);
					await LoggerService.LogActivity('Next page loaded');
					await scrapPage(puppeteerPage, scrapNextPage);
				}
			};

			const page = await browser.newPage();

			await LoggerService.LogActivity('Loading godaddy');
			/*await doLogin(page);
			await LoggerService.LogActivity('Login success');*/

			await page.goto('https://auctions.godaddy.com/');
			await System.sleep(5000);

			await LoggerService.LogActivity('Set list to show 500 rows per page');
			await setRowsShownTo500(page);

			await LoggerService.LogActivity('Scraping godaddy');
			await scrapPage(page);

			await LoggerService.LogActivity('Scraping completed');

			await browser.close();
			return goDaddyDomainData;
		} catch (err) {
			await browser.close();
			await LoggerService.LogActivity('GoDaddy scrap error', err);
			throw err;
		}
	}
	static async getBidUrl(domainName: string): Promise<string> {
		domainName = domainName.trim();
		const browser = await puppeteer.launch({
			args: ['--no-sandbox'],
		});

		try {

			const gotoDetailsPage = async (puppeteerPage: puppeteer.Page) => {
				let domainFound: any = await puppeteerPage.evaluate((domainNameEvaluate) => {
					const domains = Array.from(document.querySelectorAll('#search-table > tbody > tr.srRow2 > td > span'));
					for(const domain of domains){
						if (domain?.textContent == domainNameEvaluate){
							const clickableDomain = domain as HTMLElement;
							clickableDomain.click();
							return true;
						}
					}
					return false;
				}, domainName);
				if (!domainFound) return {};
				await UtilService.Sleep(2000);
				domainFound = await puppeteerPage.evaluate((domainNameEvaluate) => {
					const hrefs = Array.from(document.querySelectorAll('#search-table > tbody > tr > td a'));
					for(const href of hrefs){
						const url =  href.getAttribute("href")
						if (url && (url.startsWith('trpItemListing') || url.startsWith('http'))){
							const clickableHref = href as HTMLElement;
							clickableHref.click();
							return true;
						}
					}
					return false;
				}, domainName);
				await UtilService.Sleep(2000);
				if (!domainFound) return {};

				return {
					bidUrl: puppeteerPage.url()
				}
			};


			const page = await browser.newPage();

			await page.goto(`https://auctions.godaddy.com`);
			await UtilService.Sleep(5000);
			await page.type('#txtKeywordContext', domainName);
			await page.click('#searchArea button');


			await UtilService.Sleep(3000);
			const {bidUrl} = await gotoDetailsPage(page);

			browser.close();
			return bidUrl;
		} catch (err) {
			await browser.close();
			await LoggerService.LogActivity('GoDaddy scrap error', err);
		}
		return '';
	}
	static async updateBidUrlAll(domainDataList: Record<string, DomainData>): Promise<void> {
		for(const domainData of Object.values(domainDataList)){
			this.updateBidUrl(domainData);
		}
	}
	static async updateBidUrl(domainData: DomainData): Promise<void> {
		if (domainData.bidUrl && domainData.bidUrl != '-') return;

		let bidUrl =  await this.getBidUrl(domainData.name);
		if (!bidUrl) return;

		if(bidUrl.includes('?')){
			bidUrl = bidUrl.split('?')[0];
		}

		if(bidUrl.includes('godaddy') && bidUrl.includes('auctions')){
			if(bidUrl.includes('-')){
				if (domainData.vendorId == '-' || !domainData.vendorId){
					domainData.vendorId = bidUrl.split('-').slice(-1)[0];
				}
			}
			if (domainData.vendorName == '-' || !domainData.vendorName){
				domainData.vendorName = 'GODADDY_AUCTIONS';
			}
		}

		domainData.bidUrl = bidUrl;
		domainData.save();
	}

	static async getAuctionDetails(url: string): Promise<{
		timeEndDate: Date,
		latestPriceText: string,
		latestPrice: number,
		deliveryDays: number
	}> {
		const browser = await puppeteer.launch({
			args: ['--no-sandbox'],
		});

		try {

			const getTimeEndDate = async (puppeteerPage: puppeteer.Page) => {
				const { bidEndsAt, latestPriceText, deliveryDays } = await puppeteerPage.evaluate(() => {
					const bidEndsAtText = document.querySelector('#top-card strong')?.textContent ?? '';
					const latestPriceTextRaw = document.querySelector('.currentBidPrice-text .override-buynowprice-headline strong')?.textContent ?? '';
					const latestDeliveryDaysText1 = document.querySelector('#details-card-transfer-time-text span p')?.textContent ?? '';
					const latestDeliveryDaysText2 = document.querySelector('#details-card-transfer-time-text')?.textContent ?? '';

					return {
						bidEndsAt: bidEndsAtText.replace('Ends at ', '').split(')')[0] + ')',
						latestPriceText: latestPriceTextRaw,
						deliveryDays: (latestDeliveryDaysText1 ? latestDeliveryDaysText1 : latestDeliveryDaysText2).split(' days')[0].split(' ').slice(-1).pop().split('-')[0],
						latestDeliveryDaysText1,
						latestDeliveryDaysText2
					};
				});

				return {
					timeEndDate: bidEndsAt ? new Date(bidEndsAt) : null,
					latestPriceText,
					latestPrice: UtilService.StringToInt(latestPriceText),
					deliveryDays: UtilService.normalizeNumberInt(deliveryDays)
				}
			};


			const page = await browser.newPage();

			await page.goto(url);

			await UtilService.Sleep(5000);
			const { timeEndDate, latestPriceText, latestPrice, deliveryDays } = await getTimeEndDate(page);

			browser.close();
			return { timeEndDate, latestPriceText, latestPrice, deliveryDays };
		} catch (err) {
			await browser.close();
			await LoggerService.LogActivity('GoDaddy scrap error', err);
		}
		return null;
	}
	static async updateAuctionDetails(domainData: DomainData): Promise<void> {
		if (!domainData.bidUrl || domainData.bidUrl == '-' || domainData.bidUrl.includes('expireddomains.net')) {
			if (domainData.bidUrl && domainData.bidUrl.includes('expireddomains.net')) domainData.bidUrl = null;
			await this.updateBidUrl(domainData);
			if (!domainData.bidUrl || domainData.bidUrl == '-' || domainData.bidUrl.includes('expireddomains.net')) return;
		}

		const auctionDetails =  await this.getAuctionDetails(domainData.bidUrl);
		if (!auctionDetails) return;

		if (auctionDetails.timeEndDate && moment(auctionDetails.timeEndDate).isValid()) {
			domainData.timeEndDate = auctionDetails.timeEndDate;
		}
		if (auctionDetails.latestPrice) domainData.latestPrice = auctionDetails.latestPrice;
		if (auctionDetails.latestPriceText) domainData.latestPriceText = auctionDetails.latestPriceText;
		if (auctionDetails.deliveryDays) domainData.deliveryDays = auctionDetails.deliveryDays;
		domainData.save();
	}
}

export default GoDaddyService;
