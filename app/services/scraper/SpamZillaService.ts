import * as moment from 'moment';
import * as puppeteer from 'puppeteer';
import { Env } from '../../core';
import { DomainData } from '../../models/DomainData';
import { GoDaddyDomainData, SpamZillaDomainData, SpamZillaDomainFilteredData } from '../../types/Scraper';
import DomainDataService from '../DomainDataService';
import LoggerService from '../LoggerService';
import UtilService from '../UtilService';
import BaseScraperService from './BaseScraperService';
import { Constant } from '../../common/constant/Constant';
class SpamZillaService extends BaseScraperService {
	static async ScrapDomainData(domainDataList: Record<string, DomainData>): Promise<void> {
		const browser = await puppeteer.launch({
			args: ['--no-sandbox'],
		});
		const spamZillaData: Record<string, SpamZillaDomainData> = {};
		try {
			const applySearch = async (puppeteerPage: puppeteer.Page, domainName: string) => {
				const searchField = await puppeteerPage.$('#w1 > li:nth-child(1) > input');
				await searchField.click({ clickCount: 3 });
				await searchField.type(domainName);
				await puppeteerPage.click('#w1 > li:nth-child(2) > a');
				await puppeteerPage.waitForNetworkIdle();
			};

			const lookupDomain = async (puppeteerPage: puppeteer.Page, domainNames: string[], breakTime = 5000) => {
				for (const domainName of domainNames) {
					await LoggerService.LogActivity(`SpamZilla scraping data for ${domainName}`);
					try {
						await puppeteerPage.goto('https://www.spamzilla.io/domains/', { waitUntil: 'networkidle0' });
						await applySearch(puppeteerPage, domainName);
						const matchingRow = await findMatchingRow(puppeteerPage, domainName);

						spamZillaData[domainName] = matchingRow
							? await getDetailsFromRow(matchingRow)
							: { da: '-', pa: '-', ss: '-', age: '-', tf: '-', cf: '-', alexaRank: '-' };
						await LoggerService.LogActivity(
							`SpamZilla data for ${domainName}: ${JSON.stringify(spamZillaData[domainName])}`
						);
						if (breakTime > 0) {
							await UtilService.Sleep(breakTime);
						}
					} catch (err) {
						await this.TakeScreenshot(
							puppeteerPage,
							`./errors/${moment().format('YYYYMMDDHHmmss')}-spamzilla-error-${domainName}.png`
						);
						await LoggerService.LogActivity(`SpamZilla scraping error on ${domainName}`, err);
					}
				}
			};

			const lookupInMultipleTabs = async (domainName: string[], tabCount = 5) => {
				const groupedDomainNames = this.SplitIntoGroups(domainName, tabCount);
				await Promise.all(
					groupedDomainNames.map(async (domainNames) => {
						const pageForGroup = await browser.newPage();

						await lookupDomain(pageForGroup, domainNames);
					})
				);
			};

			const findMatchingRow = async (puppeteerPage: puppeteer.Page, domainName: string, pageCount = 1) => {
				await LoggerService.LogActivity(`Looking up ${domainName} on page ${pageCount}`);
				const [evenRows, oddRows] = await Promise.all([
					puppeteerPage.$x('//tr[@class="even expired-domains"]'),
					puppeteerPage.$x('//tr[@class="odd expired-domains"]'),
				]);
				const rows = [...evenRows, ...oddRows];
				let matchingRow = null;
				for (const row of rows) {
					if (matchingRow === null) {
						const rowText = await row.evaluate((data) => data.querySelector('td:nth-child(2)').textContent);
						if (rowText.toLowerCase() === domainName.toLowerCase()) {
							matchingRow = row;
						}
					}
				}

				if (matchingRow === null) {
					const btnNextPage = await puppeteerPage.evaluate(() =>
						document.querySelector('#expired-domains > div > div.panel-footer > div.kv-panel-pager > ul > li.next > a')
					);
					if (btnNextPage !== null && pageCount < 5) {
						await puppeteerPage.click('#expired-domains > div > div.panel-footer > div.kv-panel-pager > ul > li.next > a');
						await puppeteerPage.waitForNetworkIdle();
						matchingRow = await findMatchingRow(puppeteerPage, domainName, pageCount + 1);
					}
				}

				return matchingRow;
			};

			const getDetailsFromRow = async (row: puppeteer.ElementHandle<Element>) => {
				return row.evaluate((data) => {
					return {
						da: data.querySelector('td.text-center.td_moz_da.expired-domains > a')?.textContent || '0',
						pa: data.querySelector('td.text-center.td_moz_pa.expired-domains')?.textContent || '0',
						ss:
							data.querySelector('td.text-center.td_sz_score.expired-domains > a.sz-details > strong')?.textContent ||
							'0',
						age: data.querySelector('td.text-center.td_age.expired-domains')?.textContent || '0',
						tf: data.querySelector('td.text-center.td_majestic_tf.expired-domains > a')?.textContent || '0',
						cf: data.querySelector('td.text-center.td_majestic_cf.expired-domains')?.textContent || '0',
						alexaRank:
							data.querySelector('td.text-center.td_alexa_rank.expired-domains > a > span')?.textContent || '-',
					};
				});
			};

			const updateDomainData = async () => {
				for(const domainName in domainDataList){
					if (!spamZillaData[domainName]) continue;


				}}
			;

			const page = await browser.newPage();

			await LoggerService.LogActivity('Loading SpamZilla');
			await page.goto('https://www.spamzilla.io/account/login/', { waitUntil: 'networkidle0' });

			await LoggerService.LogActivity('Logging in');
			await page.type('#loginform-email', Env().spamzilla.email);
			await page.type('#loginform-password', Env().spamzilla.password);
			await page.click('button[type="submit"]');
			await page.waitForNavigation({ waitUntil: 'networkidle0' });
			await LoggerService.LogActivity('Login complete');

			await LoggerService.LogActivity('Scraping SpamZilla');
			await lookupInMultipleTabs(Object.keys(domainDataList));
			await LoggerService.LogActivity('SpamZilla scraping completed');

			await browser.close();

			await this.updateToDomainData(domainDataList, spamZillaData);
		} catch (err) {
			await LoggerService.LogActivity('SpamZilla scraping failed', err);
			await browser.close();
			throw err;
		}
	}

	static async ScrapDomainListData(domainDataList: Record<string, DomainData>): Promise<Record<string, DomainData>> {
		const fs = require('fs')
		const { parse } = require('csv-parse');
		const path = require('path');
		const getMostRecentFile = (dir) => {
			const files = orderReccentFiles(dir);
			return files.length ? files[0] : undefined;
		};
		const downloadDir = path.join(__dirname, "../../../../download");

		const orderReccentFiles = (dir) => {
			return fs.readdirSync(dir)
				.filter(file => fs.lstatSync(path.join(dir, file)).isFile())
				.map(file => ({ file, mtime: fs.lstatSync(path.join(dir, file)).mtime }))
				.sort((a, b) => b.mtime.getTime() - a.mtime.getTime());
		};

		const browser = await puppeteer.launch({
			args: ['--no-sandbox'],
		});
		const page = await browser.newPage();

		const client = await page.target().createCDPSession();
		await client.send('Page.setDownloadBehavior', {
			behavior: 'allow', downloadPath: downloadDir
		});
		try {
			const applyFilter = async (puppeteerPage: puppeteer.Page, filterType: string) => {
				await puppeteerPage.click('#w1 > li:nth-child(5) > a');

				if (filterType == 'HIGH_BACKLINK'){
					for(const majesticSection of [
						[1,8],
						[2,5],
						[4,800],
						[5,50],
					]){
						const fieldIndex = majesticSection[0];
						const fieldValue = majesticSection[1];
						const majesticField = await puppeteerPage.$(`#sz-filters > div > form > div > div.col-lg-6.col-md-8.col-sm-6 > div:nth-child(2) > div:nth-child(1) > div.well.well-sm.mb-sm > ul > li:nth-child(${fieldIndex}) > div > div > div > input:nth-child(1)`);
						await majesticField.click({ clickCount: 3 });
						await majesticField.type(`${fieldValue}`);
					}
				} else if (filterType == 'HIGH_SEMRUSH_TRAFFIC'){
					const semrushField = await puppeteerPage.$(`[name="Filter[semrush_traffic_from]"]`);
					await semrushField.click({ clickCount: 3 });
					await semrushField.type('300');
				} else if (filterType == 'HIGH_DA'){
					const semrushField = await puppeteerPage.$(`[name="Filter[moz_da_from]"]`);
					await semrushField.click({ clickCount: 3 });
					await semrushField.type('50');
				}

				const field = await puppeteerPage.$(`[name="Filter[added_period]"][value="last_24_hours"]`);
				await field.click({ clickCount: 3 });

				for(const expiredTabIndex of [1,2,3]){
					const expiredTabElement = await puppeteerPage.$(`#expired-sources > div.data-sources-cnt > ul > li:nth-child(${expiredTabIndex}) > input[type=checkbox]`);
					await expiredTabElement.click({ clickCount: 1 });
				}
				const marketTab = await puppeteerPage.$('#sz-filters > div > form > div > div.col-lg-3.col-md-12.col-sm-6 > div > div:nth-child(2) > div.panel.with-nav-tabs.panel-primary.mb0 > div.panel-heading > ul > li:nth-child(2) > a');
				await marketTab.click({ clickCount: 1 });
				await page.waitForTimeout(300);

				for(const marketplaceTabIndex of [3,4,5,6,7,8,9,10,11,12,13]){
					const marketplaceTabElement = await puppeteerPage.$(`#marketplace-sources > div.data-sources-cnt > ul > li:nth-child(${marketplaceTabIndex}) > input[type=checkbox]`);
					await marketplaceTabElement.click({ clickCount: 1 });
				}
				await puppeteerPage.click('#apply-filter-btn');

				await puppeteerPage.waitForNetworkIdle();
			};

			const downloadDomainData = async (puppeteerPage: puppeteer.Page) => {
				await puppeteerPage.click('body > div.wrapper > aside > div > nav > ul > li:nth-child(4) > a');
				await page.waitForTimeout(3000);

				await puppeteerPage.select('#exportModal > div > div > div.modal-body > form > div.export-select > div > select', '10000');
				await page.waitForTimeout(3000);

				await puppeteerPage.click('#exportModal > div > div > div.modal-body > form > a:nth-child(4)');
				await page.waitForTimeout(10000);
				// await puppeteerPage.waitForNetworkIdle();
			};

			await LoggerService.LogActivity('Loading SpamZilla');
			// await page.setViewport({ width: 1366, height: 768});

			await page.goto('https://www.spamzilla.io/account/login/', { waitUntil: 'networkidle0' });

			await LoggerService.LogActivity('Logging in');
			await page.type('#loginform-email', Env().spamzilla.email);
			await page.type('#loginform-password', Env().spamzilla.password);
			await page.click('button[type="submit"]');
			await page.waitForNavigation({ waitUntil: 'networkidle0' });
			await LoggerService.LogActivity('Login complete');

			await LoggerService.LogActivity('Scraping SpamZilla Filter');
			for(const filterType of ['HIGH_SEMRUSH_TRAFFIC', 'HIGH_DA', 'HIGH_BACKLINK']){
				await page.goto('https://www.spamzilla.io/domains/', { waitUntil: 'networkidle0' });
				await LoggerService.LogActivity(`Apply Filter SpamZilla ${filterType}`);
				await applyFilter(page, filterType);
				await LoggerService.LogActivity(`Download Domain Data SpamZilla ${filterType}`);
				await downloadDomainData(page);
				await page.waitForTimeout(25000);
				await LoggerService.LogActivity(`Download Domain Data Complete SpamZilla ${filterType}`);

				const lastFile = await getMostRecentFile(downloadDir);
				// tslint:disable-next-line:only-arrow-functions
				const getData = new Promise(function(resolve, reject) {

					const data = require("fs").readFileSync(downloadDir+'/'+lastFile['file'], "utf8")
					parse(data, (err, dataParser) => {

						for (const csvData of dataParser) {
							// console.log(csvData)
							if(csvData[0]!='Name'&&csvData[0].replace(/[^\d.-]/g, '').length<3){
								const goDaddyDomainData = domainDataList[csvData[0]];
								if(!goDaddyDomainData){
									domainDataList[csvData[0]] = DomainData.build({
										name: csvData[0],
										latestPrice: UtilService.StringToInt(csvData[33]),
										latestPriceText: csvData[33],
										timeLeft: csvData[34]===''?'-':DomainDataService.dateToTimeLeft(csvData[34]),
										timeEndDate: moment(csvData[34], 'MM/DD/YYYY HH:mm:ss').toDate(),
										da: csvData[16],
										pa: csvData[17],
										ss: csvData[22],
										age: csvData[18],
										tf: csvData[2],
										cf: csvData[3],
										alexaRank: csvData[31],
										vendorId: null,
										vendorName: csvData[1].replaceAll(' ', '_').replaceAll('(', '').replaceAll(')', '').replaceAll('.', "_").toUpperCase(),
										aggregator: Constant.AGGREGATOR.SPAMZILLA,
										forceCheck: filterType == 'HIGH_BACKLINK' ? false : true
									});
								}else{
									domainDataList[csvData[0]].da = csvData[16];
									domainDataList[csvData[0]].pa = csvData[17];
									domainDataList[csvData[0]].ss = csvData[22];
									domainDataList[csvData[0]].age = csvData[18];
									domainDataList[csvData[0]].tf = csvData[2];
									domainDataList[csvData[0]].cf = csvData[3];
									domainDataList[csvData[0]].alexaRank = csvData[31];
								}
							}
						}
					}).on('end', () => {
						resolve({})
					});
				});
				await getData;
				await LoggerService.LogActivity(`SpamZilla scraping completed ${filterType}`);
			}

			await browser.close();
			return domainDataList;

			// await this.updateToDomainData(domainDataList, spamZillaData);
		} catch (err) {
			// await LoggerService.LogActivity('SpamZilla scraping failed', err);
			await browser.close();
			throw err;
		}
	}

	private static async updateToDomainData(domainDataList: Record<string, DomainData>, spamZillaData: Record<string, SpamZillaDomainData>): Promise<void> {
		for (const domainName of Object.keys(spamZillaData)) {
			const domainData = domainDataList[domainName];
			domainData.da = spamZillaData[domainName].da;
			domainData.pa = spamZillaData[domainName].pa;
			domainData.ss = spamZillaData[domainName].ss;
			domainData.age = spamZillaData[domainName].age;
			domainData.tf = spamZillaData[domainName].tf;
			domainData.cf = spamZillaData[domainName].cf;
			domainData.alexaRank = spamZillaData[domainName].alexaRank;
		}
	}
}

export default SpamZillaService;
