import * as moment from 'moment';
import * as puppeteer from 'puppeteer';
import { Env } from '../../core';
import { AxiosService } from '../../core/service/AxiosService';
import { StringUtils } from '../../core/util/StringUtils';
import { DataWebSeoChecker } from '../../models/DataWebSeoChecker';
import { DomainData } from '../../models/DomainData';
import { SpamZillaDomainData } from '../../types/Scraper';
import LoggerService from '../LoggerService';
import UtilService from '../UtilService';
import BaseScraperService from './BaseScraperService';

export class AgeService extends BaseScraperService {
	static browser;
	static page;
	public static async updateDomainAge(domainDataList: Record<string, DomainData>): Promise<void>{
		if (!this.browser) {
			this.browser = await puppeteer.launch({
				args: ['--no-sandbox'],
			});
		}
		if (!this.page) this.page = await this.browser.newPage();

		for(const domainData of Object.values(domainDataList)){
			if (!domainData.age || ['-', '0'].includes(domainData.age)){
				let age = await this.callIpLocation(domainData.name);
				// if (age == '-')  age = await this.callPrePostSeo(domainData.name);
				domainData.age = age;
			}
		}

	}
	private static async callIpLocation(domainName: string){
		const data = {
			domain: domainName,
			submit: 'Submit'
		};

		let age = '-';
		const inlineParams = StringUtils.jsonToInlineParams(data);
		const url = `${Env().ipLocation.url}?${inlineParams}`;
		try {
			const result = await AxiosService.get(url);
			const htmlResponse = result.data;
			const ageText = htmlResponse.split('<th>Domain Age</th>')[1].split('</strong></td>')[0].split('<td><strong>')[1];
			const yearsOnly = ageText.split(' years')[0];
			const daysOnly =  ageText.split(' days')[0].split(',')[1];
			const ageInYears = parseInt(yearsOnly) + parseFloat( (parseInt(daysOnly) / 365 ).toFixed(2) );
			age = `${ageInYears}`;
		} catch (err) {
			console.error(`IpLocation Error ${domainName}`);
			// console.error('IpLocationAge Error', err);
		}


		return age == 'NaN' ? '-' : age;

	}
	private static async callPrePostSeo(domainName: string){
		await this.page.goto(Env().prepostseo.url, { waitUntil: 'domcontentloaded' });
		await this.page.type('#urls', domainName);
		await this.page.click('#checkBtn', {waitUntil: 'domcontentloaded'});
		const age = await this.page.evaluate(() => {
			try {
				// document.querySelector('#checkBtn').cclick();

				const tds = Array.from(document.querySelectorAll('#resultsTable tbody td'));
				const textAge = tds[2].textContent;
				let ageNumber = 0;
				for (const textAgeToken of textAge.split(',')) {
					const value = textAgeToken.split(' ')[0];
					const denom = textAgeToken.split(' ')[1];
					if (denom == 'Years') ageNumber += parseInt(value);
					if (denom == 'Months') ageNumber += parseInt(value) / 12;
					if (denom == 'Days') ageNumber += parseInt(value) / 365;
				}
				if (isNaN(ageNumber)) return '-';
				return ageNumber;
			} catch(e){
				return '-';
			}
		});

		return age;

	}

}
