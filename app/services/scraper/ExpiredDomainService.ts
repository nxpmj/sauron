import * as moment from 'moment';
import * as puppeteer from 'puppeteer';
import { Env } from '../../core';
import { DomainData } from '../../models/DomainData';
import { GoDaddyDomainData, SpamZillaDomainData, SpamZillaDomainFilteredData } from '../../types/Scraper';
import DomainDataService from '../DomainDataService';
import LoggerService from '../LoggerService';
import UtilService from '../UtilService';
import BaseScraperService from './BaseScraperService';
import { Constant } from '../../common/constant/Constant';
import { StringUtils } from '../../core/util/StringUtils';
class ExpiredDomainService extends BaseScraperService {
	static async ScrapDomainListData(domainDataList: Record<string, DomainData>): Promise<Record<string, DomainData>> {

		const browser = await puppeteer.launch({
			args: ['--no-sandbox'],
			// headless: false,
		});
		const page = await browser.newPage();
		await page.setUserAgent("Mozilla/5.0 (X11; Linux i686; rv:98.0) Gecko/20100101 Firefox/98.0");
		try {
			const getListDomain = async (puppeteerPage: puppeteer.Page, vendorName, filter: any) => {
				const extraFilter = StringUtils.jsonToInlineParams(filter);
				await puppeteerPage.goto(`https://member.expireddomains.net/domains/${vendorName}/?fmincharcount=1&fminnumbercount=0&fmaxnumbercount=2&fpriceto=15000&flast12=1&${extraFilter}`, {
					waitUntil: 'networkidle0',
					timeout: 0,
				});
				const time1 = Math.floor(Math.random() * (4 - 2 + 1)) + 2

				await page.waitForTimeout(time1*1000);
				const btnNextPage = (await page.$('#listing > div.infos.form-inline > strong:nth-child(2)'));
				if(btnNextPage === null){
					return
				}
				const marketNames = Constant.EXPIRED_DOMAIN_MARKET_NAMES
				// tslint:disable-next-line:no-shadowed-variable
				const data = await page.evaluate((marketNames, vendorName) => {
					const columnNames = [];
					const dataList: Record<string, any> = {}
					const ths = Array.from(document.querySelectorAll('#listing > table > thead > tr > th'));
					for (const th of ths){
						columnNames.push((th.querySelector('a')?.textContent));
					}
					const hasBids = columnNames.some(e => e === 'Bids')
					const hasListingType = columnNames.some(e => e === 'Listing Type')
					const hasEndDate = columnNames.some(e => e === 'End Date')||columnNames.some(e => e === 'Endtime')

					const trs = Array.from(document.querySelectorAll('#listing > table > tbody > tr'));

					for (const tr of trs){

						const tds = Array.from( tr.querySelectorAll('td') );
						let dateString = "-"
						let sourceName;
						let isMakeOffer = false;
						let type = ''
						const nameColumn = tds[0].querySelector('a')
						const price = (tds[columnNames.indexOf("Price")].querySelector('a')?.textContent)
						if(hasEndDate){
							dateString = columnNames.some(e => e === 'End Date')? tds[columnNames.indexOf("End Date")].querySelector('a')?.textContent : tds[columnNames.indexOf("Endtime")].querySelector('a')?.textContent
						}
						if(hasListingType&&vendorName.includes("godaddy")){
							type = tds[columnNames.indexOf("Listing Type")].querySelector('a')?.textContent
							sourceName =  type !=='Bid'?"GODADDY_BUY_NOW":"GODADDY_AUCTIONS"
							if((type !== 'Buy Now' && type !=='Bid')){
								isMakeOffer = true
							}
						}else{
							type = hasBids? "Bid" : 'Buy Now'
							sourceName = marketNames[vendorName]
						}
						if(price === "Make Offer"){
							isMakeOffer  = true
						}

						if(!isMakeOffer) {
							const latestPriceText = hasBids && type==='Bid'?price:"";
							dataList[nameColumn?.textContent.toLowerCase()] = {
								name: nameColumn?.textContent.toLowerCase(),
								bidCount: hasBids ? (tds[columnNames.indexOf("Bids")].querySelector('a')?.textContent) : "-",
								latestPriceText,
								buyNowPrice: hasBids && type==='Bid'?"":price,
								timeEndDate: null,
								timeValue : dateString,
								bidUrl: nameColumn.href,
								tf: (tds[columnNames.indexOf("TF")].querySelector('a')?.textContent),
								cf: (tds[columnNames.indexOf("CF")].querySelector('a')?.textContent),
								alexaRank: (tds[columnNames.indexOf("Alexa")].querySelector('a')?.textContent),
								vendorId: null,
								vendorName: sourceName,
								forceCheck: true
							};
						}
					}
					return dataList
				}, marketNames, vendorName);
				for(const domainNames of Object.keys(data)) {
					const goDaddyDomainData = domainDataList[domainNames];
					if (!goDaddyDomainData) {
						let timeLeft = "-";
						let timeEndDate = null;

						data[domainNames].latestPrice = UtilService.StringToInt(data[domainNames].latestPriceText);
						if(data[domainNames]['timeValue']!=="-"){
							if(moment(data[domainNames]['timeValue'], "YYYY-MM-DD", true).isValid()){
								timeEndDate = moment(data[domainNames]['timeValue'], 'YYYY-MM-DD').toDate()
								timeLeft = DomainDataService.dateToTimeLeft(data[domainNames]['timeValue'])
							}else{
								timeEndDate = DomainDataService.timeLeftToDate(data[domainNames]['timeValue'])
								timeLeft = data[domainNames]['timeValue']
							}
						}
						data[domainNames].timeEndDate = timeEndDate
						data[domainNames].timeLeft = timeLeft
						delete data[domainNames]['timeValue']
						domainDataList[domainNames] = DomainData.build(data[domainNames]);
					}
					domainDataList[domainNames].aggregator = Constant.AGGREGATOR.EXPIRED_DOMAINS
				}
				const time = Math.floor(Math.random() * (6 - 3 + 1)) + 3
				await page.waitForTimeout(time*1000);
				await puppeteerPage.waitForNetworkIdle();
			};


			await LoggerService.LogActivity('Loading ExpiredDomain');
			await page.goto('https://www.expireddomains.net/login/', { waitUntil: 'networkidle0' });

			await LoggerService.LogActivity('Logging in');
			await page.type('#inputLogin', Env().expireddomain.email);
			await page.type('#inputPassword', Env().expireddomain.password);
			await page.click('#content > div > div:nth-child(1) > div > div.box-content > form > div:nth-child(4) > div > button');
			// await page.waitForNavigation({ waitUntil: 'networkidle0' });
			await LoggerService.LogActivity('Login complete');
			await page.waitForTimeout(3000);

			// await LoggerService.LogActivity('Scraping SpamZilla');
			// await page.goto('https://www.spamzilla.io/domains/', { waitUntil: 'networkidle0' });
			// await LoggerService.LogActivity('Apply Filter SpamZilla');
			for(const name of Object.keys(Constant.EXPIRED_DOMAIN_MARKET_NAMES)){
				await getListDomain(page, name, { fsrustmin: 300, o: 'semrush_us_organic_traffic'});
				await getListDomain(page, name, { fsruscmin: 300, o: 'semrush_us_organic_cost'});
				await getListDomain(page, name, { fsruskmin: 500, o: 'semrush_us_organic_keywords'});
				await getListDomain(page, name, { fmseocf: 30, o: 'majesticseo_cf'});
				await getListDomain(page, name, { fmseotf: 30, o: 'majesticseo_tf'});
			}
			await LoggerService.LogActivity('Download Domain Data ExpiredDomain');

			await LoggerService.LogActivity('Download Domain Data Complete ExpiredDomain');


			// await getData;
			// await LoggerService.LogActivity('SpamZilla scraping completed');
			await LoggerService.LogActivity('Filter domain data from existing data');

			await browser.close();
			return domainDataList;

			// await this.updateToDomainData(domainDataList, spamZillaData);
		} catch (err) {
			// await LoggerService.LogActivity('SpamZilla scraping failed', err);
			await browser.close();
			throw err;
		}
	}
}

export default ExpiredDomainService;
