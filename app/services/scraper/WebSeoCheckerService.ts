import { SpamZillaDomainData } from '../../types/Scraper';
import { Env } from '../../core';
import * as moment from 'moment';
import * as puppeteer from 'puppeteer';
import BaseScraperService from './BaseScraperService';
import LoggerService from '../LoggerService';
import UtilService from '../UtilService';
import { DomainData } from '../../models/DomainData';
import { AxiosService } from '../../core/service/AxiosService';
import { StringUtils } from '../../core/util/StringUtils';
import { DataWebSeoChecker } from '../../models/DataWebSeoChecker';

export class WebSeoCheckerService extends BaseScraperService {
	public static async updateMozData(domainDataList: Record<string, DomainData>): Promise<void>{
		const filteredNoData: DomainData[] = Object.values(domainDataList).filter(domainData => !domainData.dataWebSeoChecker);

		const loopCount = Math.ceil( filteredNoData.length / 100);
		for(let i = 0; i <loopCount; i++){
			const dataToProcess = filteredNoData.slice(i * 100, (i+1) * 100 );
			const result = await this.callApi(dataToProcess);

			const resultByDomain = {}
			result.forEach(data => resultByDomain[data.URL] = data);

			for(const domainData of dataToProcess){
				const dataToCreate = resultByDomain[domainData.name];
				if (!dataToCreate) continue;
				const dataWebSeoChecker = await DataWebSeoChecker.create({
					domainDataId: domainData.id,
					categories: dataToCreate['Categories'],
					da: dataToCreate['Domain Authority'],
					pa: dataToCreate['Page Authority'],
					totalBacklinks: dataToCreate['Total backlinks'],
					qualityBacklinks: dataToCreate['Quality backlinks'],
					qualityBacklinksPercentage: dataToCreate['quality backlinks percentage'],
					mozTrust: dataToCreate['MozTrust'],
					spamScore: dataToCreate['Spam Score'],
					trustFlow: dataToCreate['Trust flow'],
					citationFlow: dataToCreate['Citation flow'],
					indexedUrls: dataToCreate['Indexed URLs'],
					doFollowLinksPercentage: dataToCreate['DoFollow links'],
					noFollowLinksPercentage: dataToCreate['NoFollow links'],
				});
				await this.updateDomainDataWithWebSeoChecker(domainData, dataWebSeoChecker);
			}
		}

	}
	public static async callApi(listDomainData: DomainData[]): Promise<any[]>{
		const data = {
			api_key: Env().websiteSeoChecker.apiKey,
			items: listDomainData.length
		};

		let counter = 0;
		listDomainData.forEach(domainData => {
			data[`item${counter}`] = domainData.name
			counter++;
		});

		let result = { data: []};
		const inlineParams = StringUtils.jsonToInlineParams(data);
		const url = `${Env().websiteSeoChecker.url}?${inlineParams}`;
		try {
			result = await AxiosService.get(url);
		} catch (err) {
			console.error('WebSeoChecker Error', err);
		}
		return result.data;

	}
	private static async updateDomainDataWithWebSeoChecker(domainData: DomainData, dataWebSeoChecker: any){
		if (!domainData.pa || ['-', '0'].includes(domainData.pa)){
			domainData.pa = `${dataWebSeoChecker.pa}`;
		}
		if (!domainData.da || ['-', '0'].includes(domainData.da)){
			domainData.da = `${dataWebSeoChecker.da}`;
		}
		if (!domainData.ss || ['-', '0'].includes(domainData.ss)){
			domainData.ss = `${dataWebSeoChecker.spamScore}`;
		}
		if (!domainData.tf || ['-', '0'].includes(domainData.tf)){
			domainData.tf = `${dataWebSeoChecker.trustFlow}`;
		}
		if (!domainData.cf || ['-', '0'].includes(domainData.cf)){
			domainData.cf = `${dataWebSeoChecker.citationFlow}`;
		}
		await domainData.save();

	}

}
