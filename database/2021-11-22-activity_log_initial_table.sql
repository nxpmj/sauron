CREATE DATABASE IF NOT EXISTS `sauron` USE `sauron`;

CREATE TABLE IF NOT EXISTS `ActivityLog` (
    `id` bigint unsigned NOT NULL AUTO_INCREMENT,
    `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
    `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`) USING BTREE
);