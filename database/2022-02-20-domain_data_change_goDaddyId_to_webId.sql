ALTER TABLE `domaindata`
    ADD COLUMN `vendorId` VARCHAR(255) NULL DEFAULT NULL AFTER `searchCount`;
ALTER TABLE `domaindata`
    ADD COLUMN `vendorName` VARCHAR(255) NULL DEFAULT NULL AFTER `vendorId`;