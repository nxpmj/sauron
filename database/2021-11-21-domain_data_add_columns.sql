ALTER TABLE
    `DomainData`
ADD
    COLUMN `bidCount` VARCHAR(255) NULL DEFAULT NULL
AFTER
    `name`,
ADD
    COLUMN `estimatedValue` VARCHAR(255) NULL DEFAULT NULL
AFTER
    `bidCount`,
ADD
    COLUMN `latestPrice` VARCHAR(255) NULL DEFAULT NULL
AFTER
    `estimatedValue`,
ADD
    COLUMN `bidUrl` VARCHAR(255) NULL DEFAULT NULL
AFTER
    `timeLeft`;